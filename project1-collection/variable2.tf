
variable "vpcs" {
    type = string
    description = "vpcs"
    default  = "80.1.0.0/16"
}
variable "subnts_list" {
    type = list
    description = "mysubnts"
    default = ["80.1.10.0/24" ,"80.2.20.0/24" ,"80.3.30.0/24" ,"80.4.40.0/24" ]
}
variable "az_list" {
    type = list
    description = "az_grp"
    default = [ "us-east-1a" , "us-east-1b" , "us-east-1c" , "us-east-1d" ]
}
variable "ipaddr_list" {
    type = list
    description = "ipaddrs"
    default = [ "80.1.10.100" ,"80.2.20.100" , "80.3.30.100" ,"80.4.40.100" ]
}

variable "tools1_list" {
    type = map
    description = "tools"
    default =  {
        "ami_est1" = "ami-01ca03df4a6012157"
        "inst_tp"  = "t2.micro"
    }
}
variable "tags_list" {
    type = list
    description = "identifier for servers"
    default = ["webserver-1a" , "webserver-1b" , "webserver-1c" , "webserver-1d" ]
}
variable "az_keys" {
    type = list
    description =  " keys for az-servers"
    default = [ "kyn600" ,"kyn601" , "kyn602" , "kyn603"]
}
output "vpcs_out" {
    value =var.vpcs
}
output "subnt_out" {
    value =var.subnts_list
}
output "az_out" {
    value = var.az_list
}
output "ipadd_out" {
    value = var.ipaddr_list
}
output "tools1_out" {
    value = var.tools1_list
}
output "tags_out" {
    value = var.tags_list
}
output "az_keys_out" {
    sensitive = true
    value = var.az_keys
}