provider "aws" {
    alias = "prov2"
    region = "us-east-1" 
}
# create 4 ebs  for each availability zone  
resource "aws_ebs_volume" "ebs1_est1a" {
    size = 10
    availability_zone = var.az_list[0]

}
resource "aws_ebs_snapshot" "snap1" {
    volume_id = aws_ebs_volume.ebs1_est1a.id
}
resource "aws_ebs_volume" "ebs2_est1b" {
    size = 10
    availability_zone = var.az_list[1]

}
# create  ebs snapshot  for each availability zone
resource "aws_ebs_snapshot" "snap2" {
    volume_id = aws_ebs_volume.ebs2_est1b.id
}
resource "aws_ebs_volume" "ebs3_est1c" {
    size = 10
    availability_zone = var.az_list[2]

}
resource "aws_ebs_snapshot" "snap3" {
    volume_id = aws_ebs_volume.ebs3_est1c.id
}
resource "aws_ebs_volume" "ebs4_est1d" {
    size = 10
    availability_zone = var.az_list[3]

}
resource "aws_ebs_snapshot" "snap4" {
    volume_id = aws_ebs_volume.ebs4_est1d.id
}
resource "aws_ebs_volume" "ebs10_est1a" {
    size = 10
    snapshot_id = aws_ebs_snapshot.snap1.id
    availability_zone = var.az_list[0]
}
resource "aws_ebs_volume" "ebs11_est1a" {
    size = 10
    snapshot_id = aws_ebs_snapshot.snap1.id
    availability_zone = var.az_list[0]
}
resource "aws_ebs_volume" "ebs12_est1a" {
    size = 10
    snapshot_id = aws_ebs_snapshot.snap1.id
    availability_zone = var.az_list[0]
}
resource "aws_ebs_volume" "ebs10_est1b" {
    size = 10
    snapshot_id = aws_ebs_snapshot.snap2.id
    availability_zone = var.az_list[1]
}

resource "aws_ebs_volume" "ebs11_est1b" {
    size = 10
    snapshot_id = aws_ebs_snapshot.snap2.id
    availability_zone = var.az_list[1]
}
resource "aws_ebs_volume" "ebs12_est1b" {
    size = 10
    snapshot_id = aws_ebs_snapshot.snap2.id
    availability_zone = var.az_list[1]
}
resource "aws_ebs_volume" "ebs10_est1c" {
    size = 10
    snapshot_id = aws_ebs_snapshot.snap3.id
    availability_zone = var.az_list[2]
}

resource "aws_ebs_volume" "ebs11_est1c" {
    size = 10
    snapshot_id = aws_ebs_snapshot.snap3.id
    availability_zone = var.az_list[2]
}
resource "aws_ebs_volume" "ebs12_est1c" {
    size = 10
    snapshot_id = aws_ebs_snapshot.snap3.id
    availability_zone = var.az_list[2]
}
resource "aws_ebs_volume" "ebs10_est1d" {
    size = 10
    snapshot_id = aws_ebs_snapshot.snap4.id
    availability_zone = var.az_list[3]
}

resource "aws_ebs_volume" "ebs11_est1d" {
    size = 10
    snapshot_id = aws_ebs_snapshot.snap4.id
    availability_zone = var.az_list[3]
}
resource "aws_ebs_volume" "ebs12_est1d" {
    size = 10
    snapshot_id = aws_ebs_snapshot.snap4.id
    availability_zone = var.az_list[3]
}
#  attach each ebs to the one of ec2 instances for each availabily zone
resource "aws_volume_attachment" "east1a" {
    device_name = "/dev/sda1"
    volume_id = aws_ebs_volume.ebs1_est1a.id 
    instance_id = aws_instance.inst_est10[0].id
}
resource "aws_volume_attachment" "east2a" {
    device_name = "/dev/sda2"
    volume_id =aws_ebs_volume.ebs10_est1a.id
    instance_id = aws_instance.inst_est10[1].id
}
resource "aws_volume_attachment" "east3a" {
    device_name = "/dev/sda3"
    volume_id = aws_ebs_volume.ebs11_est1a.id
    instance_id = aws_instance.inst_est10[2].id
}
resource "aws_volume_attachment" "east4a" {
    device_name = "/dev/sda4"
    volume_id = aws_ebs_volume.ebs12_est1a.id
    instance_id = aws_instance.inst_est10[3].id
}

resource "aws_volume_attachment" "east1b" {
    device_name = "/dev/sda1"
    volume_id = aws_ebs_volume.ebs2_est1b.id 
    instance_id = aws_instance.inst_est20[0].id
}
resource "aws_volume_attachment" "east2b" {
    device_name = "/dev/sda2"
    volume_id = aws_ebs_volume.ebs10_est1b.id
    instance_id = aws_instance.inst_est20[1].id
}
resource "aws_volume_attachment" "east3b" {
    device_name = "/dev/sda3"
    volume_id = aws_ebs_volume.ebs11_est1b.id
    instance_id = aws_instance.inst_est20[2].id
}
resource "aws_volume_attachment" "east4b" {
    device_name = "/dev/sda4"
    volume_id = aws_ebs_volume.ebs11_est1b.id
    instance_id = aws_instance.inst_est20[3].id
}
resource "aws_volume_attachment" "east1c" {
    device_name = "/dev/sda1"
    volume_id = aws_ebs_volume.ebs3_est1c.id 
    instance_id = aws_instance.inst_est30[0].id
}
resource "aws_volume_attachment" "east2c" {
    device_name = "/dev/sda2"
    volume_id = aws_ebs_volume.ebs10_est1c.id
    instance_id = aws_instance.inst_est30[1].id
}
resource "aws_volume_attachment" "east3c" {
    device_name = "/dev/sda3"
    volume_id = aws_ebs_volume.ebs11_est1c.id
    instance_id = aws_instance.inst_est30[2].id
}
resource "aws_volume_attachment" "east4c" {
    device_name = "/dev/sda4"
    volume_id = aws_ebs_volume.ebs12_est1c.id
    instance_id = aws_instance.inst_est30[3].id
}
resource "aws_volume_attachment" "east1d" {
    device_name = "/dev/sda1"
    volume_id = aws_ebs_volume.ebs4_est1d.id 
    instance_id = aws_instance.inst_est40[0].id
}
resource "aws_volume_attachment" "east2d" {
    device_name = "/dev/sda2"
    volume_id = aws_ebs_volume.ebs10_est1d.id
    instance_id = aws_instance.inst_est40[1].id
}
resource "aws_volume_attachment" "east3d" {
    device_name = "/dev/sda3"
    volume_id = aws_ebs_volume.ebs11_est1d.id
    instance_id = aws_instance.inst_est40[2].id
}
resource "aws_volume_attachment" "east4d" {
    device_name = "/dev/sda4"
    volume_id = aws_ebs_volume.ebs12_est1d.id
    instance_id = aws_instance.inst_est40[3].id
}
#   create  s3  bucket 
resource "aws_s3_bucket" "bckt-east1" {
    bucket = "bkt_est1_list"
    acl = "private"
    versioning {
      enabled = true
    }
    lifecycle_rule {
        enabled = true
      transition {
        days = 35
        storage_class = "STANDARD_IA"
      }
      expiration {
        days = 365
      }
    }
    logging {
      target_bucket = aws_s3_bucket.lgbket.id
    }
  
}
# create s3 bucket for logging so that can be archived 
resource "aws_s3_bucket" "lgbket" {
    bucket = "bket_estlg"
    acl = "log_delivery_write"
    versioning {
      enabled = true
    }
    lifecycle_rule {
        enabled = true
      transition {
        days = 35
        storage_class = "STANDARD_IA"
      }
      transition {
        days = 95
        storage_class = "GLACIER"
      }
      expiration {
        days = 365
      }
    }   
}