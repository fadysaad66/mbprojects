provider "aws" {
    region = "us-east-1"
   }
# create 4 ec2 instances for each availability zone 
resource "aws_instance" "inst_est10" {
    ami =var.tools1_list.ami_est1
    instance_type =var.tools1_list.inst_tp
    key_name =var.az_keys[0]
    availability_zone = var.az_list[0]
    count = 4
    tags = {
         name=var.tags_list[0]
    }
    security_groups =[aws_security_group.secgrp1-est1a.id]
    network_interface {
      device_index = 0
      network_interface_id = aws_network_interface.intf1_est1a.id
    }
}
resource "aws_instance" "inst_est20" {
    ami =var.tools1_list.ami_est1
    instance_type=var.tools1_list.inst_tp
    key_name =var.az_keys[1]
    availability_zone =var.az_list[1]
    count =4
    tags = {
        name =var.tags_list[1]
    }
    security_groups=[aws_security_group.secgrp2-est1b.id]
    network_interface {
      device_index = 0
      network_interface_id =aws_network_interface.intf2_est1b.id
    }
  
}
resource "aws_instance" "inst_est30" {
    ami = var.tools1_list.ami_est1
    instance_type =var.tools1_list.inst_tp
    key_name =var.az_keys[2]
    availability_zone =var.az_list[2]
    count = 4
    tags = {
        name=var.tags_list[2]
    }
security_groups=[aws_security_group.secgrp3-est1c.id]
network_interface {
      device_index= 0
      network_interface_id=aws_network_interface.intf3_est1c.id
    }
  
}
resource "aws_instance" "inst_est40" {
    ami = var.tools1_list.ami_est1
    instance_type =var.tools1_list.inst_tp
    key_name =var.az_keys[3]
    availability_zone =var.az_list[3]
    count = 4
    tags = {
        name=var.tags_list[3]
    }
    security_groups=[aws_security_group.secgrp4-est1d.id]
    network_interface {
      device_index= 0
      network_interface_id=aws_network_interface.intf4_est1d.id
    }
  
}

#user_data =<<EOF
#!bin/bash
#sudo yum update -y
#sudo yum  upgrade -y
#sudo yum install apache2 -y
#sudo systemctl start apache2 
#sudo yum install vftpd  -y
#sudo systemctl start vftpd
#sudo systemctl  start  httpd  echo "c welcome to my first webserver "c /var/www/index.html
#EOF
locals {
    env  = terraform.workspace
}