    provider "aws" {
    alias = "prov3"
    region = "us-east-1"
}
resource "aws_lb" "est1lb_list" {
    name ="est1lb"
    load_balancer_type = "application"
    enable_cross_zone_load_balancing = false
    enable_deletion_protection = false
    enable_http2 = true
    access_logs {
       bucket = aws_s3_bucket.lgbket.id
    }
    

    ip_address_type = "ipv4"
    subnet_mapping {
      allocation_id = aws_eip.eip1_est1a.id 
      subnet_id = aws_subnet.sbnt1_est1.id
    }
    subnet_mapping {
      allocation_id = aws_eip.eip2_est1b.id 
      subnet_id = aws_subnet.sbnt2_est1.id
    }
    subnet_mapping {
      allocation_id = aws_eip.eip3_est1c.id 
      subnet_id = aws_subnet.sbnt3_est1.id
    }
    subnet_mapping {
      allocation_id = aws_eip.eip4_est1d.id 
      subnet_id = aws_subnet.sbnt4_est1.id
    }
    security_groups = [aws_security_group.seclb.id]
}
resource "aws_security_group" "seclb" {
    name = "sclb_est1"
    vpc_id = aws_vpc.vpcs_est1.id
    ingress  {
         from_port = 80
         to_port   = 80
         protocol  = "tcp"
         cidr_blocks = ["0.0.0.0/0"]
    }
    ingress  {
         from_port = 443
         to_port   = 443
         protocol  = "tcp"
         cidr_blocks = ["0.0.0.0/0"]
    }
    ingress  {
         from_port = 21
         to_port   = 21
         protocol  = "tcp"
         cidr_blocks = ["0.0.0.0/0"]
    }
    ingress  {
         from_port = 22
         to_port   = 22
         protocol  = "tcp"
         cidr_blocks = ["0.0.0.0/0"]
    }
    egress  {
         from_port = 0
         to_port   = 0
         protocol  = "-1"
         cidr_blocks = ["0.0.0.0/0"]
    }
}
resource "aws_lb_listener" "lisweb80" {
    port = 80
    protocol = "HTTP"
    load_balancer_arn = aws_lb.est1lb_list.arn
    default_action {
      type = "forward"
    } 
}

resource "aws_lb_listener" "liswebs443" {
    port = 443
    protocol = "HTTPS"
    load_balancer_arn = aws_lb.est1lb_list.arn
    default_action {
      type = "forward"
    }
}

   resource "aws_lb_target_group" "trgt1" {
   load_balancing_algorithm_type = "round_robin"
   port = 80
   protocol = "HTTP"
   target_type = "instance"
   health_check {
     enabled = true 
     path = "/lbcheck1"
   }
   vpc_id = aws_vpc.vpcs_est1.id
   
}
   resource "aws_lb_target_group" "trgt2" {
   load_balancing_algorithm_type = "round_robin"
   port = 443
   protocol = "HTTPS"
   target_type = "instance"
   health_check {
     enabled = true 
     path = "/lbcheck2"
   }
   vpc_id = aws_vpc.vpcs_est1.id
}
resource "aws_lb" "nt_est1_lb" {
   name = "nt1lb"
   load_balancer_type = "network"
   enable_cross_zone_load_balancing = true 
   enable_deletion_protection = true
   security_groups = [aws_security_group.seclb.id ]
   ip_address_type = "ipv4"
   access_logs {
       bucket = aws_s3_bucket.lgbket.id
    }
    
    subnet_mapping {
      allocation_id =aws_eip.eip1_est1a.id 
      subnet_id = aws_subnet.sbnt1_est1.id
    }
    subnet_mapping {
      allocation_id =aws_eip.eip2_est1b.id  
      subnet_id = aws_subnet.sbnt2_est1.id
    }
    subnet_mapping {
      allocation_id = aws_eip.eip3_est1c.id 
      subnet_id = aws_subnet.sbnt3_est1.id
    }
    subnet_mapping {
      allocation_id = aws_eip.eip4_est1d.id 
      subnet_id = aws_subnet.sbnt4_est1.id
    }
}
resource "aws_lb_listener" "lisftp" {
    port = 21
    protocol = "tcp"
    load_balancer_arn =aws_lb.nt_est1_lb.arn 
    default_action {
      type = "forward"
    }  
}
resource "aws_lb_listener" "lisssh" {
    port = 22
    protocol = "tcp"
    load_balancer_arn =aws_lb.nt_est1_lb.arn 
    default_action {
      type = "forward"
    }  
}

resource "aws_lb_target_group" "trgt3" {
   load_balancing_algorithm_type = "least_outstanding_requests"
   port = 21
   protocol = "tcp"
   target_type = "instance"
   health_check {
     path = "/ftpcheck"
   }
   vpc_id = aws_vpc.vpcs_est1.id 
}
resource "aws_lb_target_group" "trgt4" {
   load_balancing_algorithm_type = "least_outstanding_requests"
   port = 22
   protocol = "tcp"
   target_type = "instance"
   health_check {
     path = "/sshcheck"
   }
   vpc_id = aws_vpc.vpcs_est1.id
  
}