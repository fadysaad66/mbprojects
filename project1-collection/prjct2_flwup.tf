provider "aws" {
    alias = "prov1"
    region = "us-east-1"
}
#   vpc for us-east-1
resource "aws_vpc" "vpcs_est1" {
    cidr_block  = "80.1.0.0/16"
    tags = {
        name = "vpcs_est-1"
    }
  
}
# create  4 subnets for each availability zone 1a,b,c,d
resource "aws_subnet" "sbnt1_est1" {
    cidr_block =var.subnts_list[0]
    availability_zone =var.az_list[0]
    vpc_id=aws_vpc.vpcs_est1.id
    tags = {
        name = "subnt-est1a"
    }

}
resource "aws_subnet" "sbnt2_est1" {
    cidr_block =var.subnts_list[1]
    availability_zone =var.az_list[1]
    vpc_id=aws_vpc.vpcs_est1.id

    tags = {
        name = "subnt-est1b"
    }

}
resource "aws_subnet" "sbnt3_est1" {
    cidr_block =var.subnts_list[2]
    availability_zone =var.az_list[2]
     vpc_id=aws_vpc.vpcs_est1.id

    tags = {
        name = "subnt-est1c"
    }

}
resource "aws_subnet" "sbnt4_est1" {
    cidr_block =var.subnts_list[3]
    availability_zone =var.az_list[3]
    vpc_id=aws_vpc.vpcs_est1.id

    tags = {
        name = "subnt-est1d"
    }

}
# create  route-table , internet gateway 
# associate each subnets  with routet-table

resource "aws_internet_gateway" "GWest1" {
    vpc_id =aws_vpc.vpcs_est1.id
  
}
resource "aws_route_table" "rtblest1" {
    vpc_id =aws_vpc.vpcs_est1.id
     route  {
         cidr_block = " 0.0.0.0/0"
         gateway_id  =aws_internet_gateway.GWest1.id
     }
}
resource "aws_route_table_association" "associate1a" {
    subnet_id =aws_subnet.sbnt1_est1.id 
    route_table_id =aws_route_table.rtblest1.id
}
resource "aws_route_table_association" "associate1b" {
    subnet_id =aws_subnet.sbnt2_est1.id 
    route_table_id =aws_route_table.rtblest1.id
}
resource "aws_route_table_association" "associate1c" {
    subnet_id =aws_subnet.sbnt3_est1.id 
    route_table_id =aws_route_table.rtblest1.id
}
resource "aws_route_table_association" "associate1d" {
    subnet_id =aws_subnet.sbnt4_est1.id 
    route_table_id =aws_route_table.rtblest1.id
}
# create 4 security groups and refrenced to ec2 intances for each availability zone  1a,b,c,d
resource "aws_security_group" "secgrp1-est1a" {
    name = "sec1a"
    vpc_id = aws_vpc.vpcs_est1.id
    ingress {
        from_port = 80
        to_port   = 80
        protocol  = "tcp"
        cidr_blocks = [var.subnts_list[0]]
    }
     ingress {
        from_port = 443
        to_port   = 443
        protocol  = "tcp"
        cidr_blocks = [var.subnts_list[0]]
    }
     ingress {
        from_port = 21
        to_port   = 21
        protocol  = "tcp"
        cidr_blocks = [var.subnts_list[0]]
    }
     ingress {
        from_port = 22
        to_port   = 22
        protocol  = "tcp"
        cidr_blocks = [var.subnts_list[0]]
    }
     egress {
        from_port = 0
        to_port   = 0
        protocol  = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
}
resource "aws_security_group" "secgrp2-est1b" {
    name = "sec1b"
    vpc_id = aws_vpc.vpcs_est1.id
    ingress {
        from_port = 80
        to_port   = 80
        protocol  = "tcp"
        cidr_blocks = [var.subnts_list[1]]
    }
     ingress {
        from_port = 443
        to_port   = 443
        protocol  = "tcp"
        cidr_blocks = [var.subnts_list[1]]
    }
     ingress {
        from_port = 21
        to_port   = 21
        protocol  = "tcp"
        cidr_blocks = [var.subnts_list[1]]
    }
     ingress {
        from_port = 22
        to_port   = 22
        protocol  = "tcp"
        cidr_blocks = [var.subnts_list[1]]
    }
     egress {
        from_port = 0
        to_port   = 0
        protocol  = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
}
resource "aws_security_group" "secgrp3-est1c" {
    name = "sec1c"
    vpc_id = aws_vpc.vpcs_est1.id
    ingress {
        from_port = 80
        to_port   = 80
        protocol  = "tcp"
        cidr_blocks = [var.subnts_list[2]]
    }
     ingress {
        from_port = 443
        to_port   = 443
        protocol  = "tcp"
        cidr_blocks = [var.subnts_list[2]]
    }
     ingress {
        from_port = 21
        to_port   = 21
        protocol  = "tcp"
        cidr_blocks = [var.subnts_list[2]]
    }
     ingress {
        from_port = 22
        to_port   = 22
        protocol  = "tcp"
        cidr_blocks = [var.subnts_list[2]]
    }
     egress {
        from_port = 0
        to_port   = 0
        protocol  = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
}
resource "aws_security_group" "secgrp4-est1d" {
    name = "sec1d"
    vpc_id = aws_vpc.vpcs_est1.id
    ingress {
        from_port = 80
        to_port   = 80
        protocol  = "tcp"
        cidr_blocks = [var.subnts_list[3]]
    }
     ingress {
        from_port = 443
        to_port   = 443
        protocol  = "tcp"
        cidr_blocks = [var.subnts_list[3]]
    }
     ingress {
        from_port = 21
        to_port   = 21
        protocol  = "tcp"
        cidr_blocks = [var.subnts_list[3]]
    }
     ingress {
        from_port = 22
        to_port   = 22
        protocol  = "tcp"
        cidr_blocks = [var.subnts_list[3]]
    }
     egress {
        from_port = 0
        to_port   = 0
        protocol  = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
}
# create  4 network  interface and  elastic ip 
resource "aws_network_interface" "intf1_est1a" {
    subnet_id = aws_subnet.sbnt1_est1.id 
    private_ips = ["80.1.10.100"]
    security_groups = [aws_security_group.secgrp1-est1a.id]
  
}
resource "aws_network_interface" "intf2_est1b" {
    subnet_id = aws_subnet.sbnt2_est1.id 
    private_ips = ["80.2.20.100"]
    security_groups = [aws_security_group.secgrp2-est1b.id]
  
}
resource "aws_network_interface" "intf3_est1c" {
    subnet_id = aws_subnet.sbnt3_est1.id 
    private_ips = ["80.3.30.100"]
    security_groups = [aws_security_group.secgrp3-est1c.id]
  
}
resource "aws_network_interface" "intf4_est1d" {
    subnet_id = aws_subnet.sbnt4_est1.id 
    private_ips = ["80.4.40.100"]
    security_groups = [aws_security_group.secgrp4-est1d.id]
  
}
resource "aws_eip" "eip1_est1a" {
    vpc = true
    network_interface = aws_network_interface.intf1_est1a.id 
    associate_with_private_ip = " 80.1.10.100"
    depends_on = [aws_internet_gateway.GWest1]

}
resource "aws_eip" "eip2_est1b" {
    vpc = true
    network_interface = aws_network_interface.intf2_est1b.id 
    associate_with_private_ip = " 80.2.20.100"
    depends_on = [aws_internet_gateway.GWest1]

}
resource "aws_eip" "eip3_est1c" {
    vpc = true
    network_interface = aws_network_interface.intf3_est1c.id 
    associate_with_private_ip = " 80.3.30.100"
    depends_on = [aws_internet_gateway.GWest1]

}
resource "aws_eip" "eip4_est1d" {
    vpc = true
    network_interface = aws_network_interface.intf4_est1d.id 
    associate_with_private_ip = " 80.4.40.100"
    depends_on = [aws_internet_gateway.GWest1]

}
