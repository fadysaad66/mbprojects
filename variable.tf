
variable "tools" {
    type =  map
    description = "tools"
    default = {
        ami_est1  = "ami-01ca03df4a6012157"
        inst_type = "t2.micro"
        keys_lst  = "kyn660"
        tags_lst  = "webserver"
    }
}
variable "netwrks" {
    type = map
    description = "mynet"
    default = {
        "vpcs"    = "10.1.0.0/16"
        "subnts"  = "10.1.10.0/24"
        "ipaddrs" = "10.1.10.100"
    }
}
output "tools" {
    value = var.tools
}
output "netwrks_out" {
    value = var.netwrks
}