provider "aws" {
    region = "us-east-1"
}
resource "aws_instance" "web1" {
    ami = var.tools.ami_est1
    instance_type = var.tools.inst_type
    key_name = var.tools.keys_lst
    tags = {
      name = var.tools.tags_lst
    }
    network_interface {
      device_index = 0
      network_interface_id = aws_network_interface.intf_est1.id
    }
    
    availability_zone = "us-east-1a"
    
  
  
}
resource "aws_vpc" "vpc_est1" {
    cidr_block = var.netwrks.vpcs
  tags = {
    name = "vpc-est1"
  }
}
resource "aws_subnet" "subnt_est1" {
    cidr_block = var.netwrks.subnts
    vpc_id = aws_vpc.vpc_est1.id
    availability_zone = "us-east-1a"
}
resource "aws_internet_gateway" "GW_est1" {
    vpc_id = aws_vpc.vpc_est1.id
}
resource "aws_route_table" "rtble_est1" {
    vpc_id = aws_vpc.vpc_est1.id  
    route  {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.GW_est1.id
    }   
  
}
resource "aws_route_table_association" "aszct_est1" {
    subnet_id = aws_subnet.subnt_est1.id
    route_table_id = aws_route_table.rtble_est1.id
  
}
resource "aws_security_group" "sec_est1" {
   name = "scest1"
   vpc_id = aws_vpc.vpc_est1.id
   ingress  {
       from_port = 80
       to_port   = 80
       protocol  = "tcp"
       cidr_blocks = [var.netwrks.subnts]
   }
   ingress  {
       from_port = 443
       to_port   = 443
       protocol  = "tcp"
       cidr_blocks = [var.netwrks.subnts]
   }
   ingress  {
       from_port = 21
       to_port   = 21
       protocol  = "tcp"
       cidr_blocks = [var.netwrks.subnts]
   }
   ingress  {
       from_port = 22
       to_port   = 22
       protocol  = "tcp"
       cidr_blocks = [var.netwrks.subnts]
   }
   egress  {
       from_port = 0
       to_port   = 0
       protocol  = "-1"
       cidr_blocks = ["0.0.0.0/0"]
   }

}

resource "aws_network_interface" "intf_est1" {
    subnet_id = var.netwrks.subnts
    private_ips = [var.netwrks.ipaddrs]
    security_groups = [aws_security_group.sec_est1.id]

}
resource "aws_eip" "pub-est1" {
    vpc = true 
    network_interface = aws_network_interface.intf_est1.id
    associate_with_private_ip = var.netwrks.ipaddrs
    depends_on =[aws_internet_gateway.GW_est1]

}
resource "aws_ebs_volume" "ebs_est1" {
    size = 10
    availability_zone = "us-east-1a"

}
resource "aws_volume_attachment" "attach_est1" {
    device_name = "/dev/sda3"
    volume_id = aws_ebs_volume.ebs_est1.id 
    instance_id = aws_instance.web1.id
  
}
#user_data =<<EOF
#!bin/bash
#sudo yum update -y
#sudo yum upgrade -y
#sudo yum install apache2
#sudo systemctl start apache2
#sudo systemctl  start  httpd  echo "c welcome to my first webserver "c /var/www/index.html
#EOF
