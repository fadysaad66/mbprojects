provider "aws" {
    alias = "west1_lb"
    region =var.regions_list[2]
    
}
# create  load balancer  type  network for us-west-1
resource "aws_lb" "west1_lb" {
    name = "west1lb"
    load_balancer_type = "network"
    enable_cross_zone_load_balancing = true
    enable_deletion_protection = true
    subnet_mapping {
      allocation_id = aws_eip.eip_wst1a.id
      subnet_id = aws_subnet.sbnt0_west1.id
    }
    subnet_mapping {
      allocation_id = aws_eip.eip_wst1b.id 
      subnet_id = aws_subnet.sbnt1_west1.id
    }
    idle_timeout =  30
    security_groups = [aws_security_group.sc_west1lb.id]
    
}

resource "aws_security_group" "sc_west1lb" {
    name = "swst1lb"
    vpc_id = aws_vpc.vpcwst1.id
      ingress  {
      from_port = 80
      to_port   = 80
      protocol ="tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }
    ingress  {
      from_port = 443
      to_port   = 443
      protocol="tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }
    ingress  {
      from_port = 22
      to_port   = 22
      protocol ="tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }
    egress  {
      from_port = 0
      to_port   = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
  }
}
resource "aws_lb_listener" "lis_west30" {
    load_balancer_arn = aws_lb.west1_lb.arn 
    port = 80
    protocol = "tcp"
    default_action {
      type = "forward"
      target_group_arn = aws_lb_target_group.trgt_wst30.arn
    }
  
}
resource "aws_lb_target_group" "trgt_wst30" {
  load_balancing_algorithm_type = "round_robin"
  protocol = "tcp"
  port = 80
  target_type = "instance"
  health_check {
    path = aws_s3_bucket.west1bkt.id
  }
  vpc_id = aws_vpc.vpcwst1.id
}
resource "aws_lb_listener" "lis_wst31" {
    load_balancer_arn = aws_lb.west1_lb.arn 
    port = 443
    protocol = "tcp"
    default_action {
      type = "forward"
      target_group_arn = aws_lb_target_group.trgt_wst31.arn
    }
  
}
resource "aws_lb_target_group" "trgt_wst31" {
  load_balancing_algorithm_type = "round_robin"
  protocol = "tcp"
  port = 443
  target_type = "instance"
  health_check {
    path = aws_s3_bucket.west1bkt.id
  }
  vpc_id = aws_vpc.vpcwst1.id
}

resource "aws_lb_listener" "lis_wst32" {
    load_balancer_arn = aws_lb.west1_lb.arn 
    port = 22
    protocol = "tcp"
    default_action {
      type = "forward"
      target_group_arn = aws_lb_target_group.trgt_wst32.arn
    }
  
}
resource "aws_lb_target_group" "trgt_wst32" {
  load_balancing_algorithm_type = "least_outstanding_requests"
  protocol = "tcp"
  port = 22
  target_type = "instance"
  health_check {
    path = aws_s3_bucket.west1bkt.id
  }
  vpc_id = aws_vpc.vpcwst1.id
}