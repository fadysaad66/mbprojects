variable "regions_list" {
    type = list
    description = " region-selection"
    default = ["us-east-1" , "us-east-2" ,"us-west-1"]
}
variable "ami_checks" {
    type = map
    description = "inst_requirements"
    default = {
         "ami_est1" = "ami-01ca03df4a6012157"
         "ami_est2" = "ami-000e7ce4dd68e7a11"
         "ami_wst1" = "ami-04179d30492b778ad"
    }
}
variable "tools" {
    type = map
    description = "tools"
    default = {
        "inst_type" = "t2.micro"
        "keys"     = "kyn959"
    }
}
variable "vpcs_list" {
    type = list
    description = "vpcs"
    default = [ "174.169.0.0/16" , "175.170.0.0/16" ,"176.171.0.0/16" ]
}

variable "subnets_list" {
    type = list
    description = "subnts"
    default = [ "174.169.10.0/24","174.169.20.0/24" , "175.170.10.0/24","175.170.20.0/24" , "176.171.10.0/24","176.171.20.0/24" ]
}
variable "ipaddr_list" {
    type = list
    description = "ipaddrs"
    default = [ "174.169.10.100" ,"174.169.20.100","175.170.10.100","175.170.20.100","176.171.10.100","176.171.20.100"]
}

variable "tags_list" {
    type = list
    description = "identifiers"
    default = [ "webserv-est1a" ,"webservest1b" , "webserv-est2a" ,"webservest2b" , "webserv-wst1a" ,"webservwst1b" ]
}
variable "az_list" {
    type = list
    description = "zones"
    default = [ "us-east-1a" ,"us-east-1b" ,"us-east2a","us-east-2b","us-west-1a","us-west1b"]
}
variable "vpcs_prng" {
    type = list
    description = "vpcs-peering"
    default = [ "50.10.0.0/16" ,"51.20.0.0/16","50.10.1.0/24","51.20.2.0/24","50.10.1.10","51.20.2.10","50.10.1.11","51.20.2.11"]
}
variable "vpcs_peering_est2to_wst1" {
    type = list
    description = "vpcs-peering2"
    default = [ "52.30.0.0/16", "52.30.3.0/24", "53.30.3.13","53.30.3.14"]
}
output "vpcs2_out" {
    value = var.vpcs_peering_est2to_wst1
}

output "prng_out" {
    value = var.vpcs_prng
}
output "ipaddrs_out" {
    value = var.ipaddr_list
}
output "tags_out" {
    value = var.tags_list
}
output "regions_out" {
    value = var.regions_list
}
output "ami_out" {
    value = var.ami_checks
}
output "vpcs_out" {
    value = var.vpcs_list
}
output "subnt_out" {
    value =var.subnets_list
}
output "tools_out" {
    value = var.tools
}