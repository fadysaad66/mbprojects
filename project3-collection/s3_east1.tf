provider "aws" {
    alias = "est1_s3"
    region =var.regions_list[0]
    
}
resource "aws_s3_bucket" "est1bkt" {
    bucket ="est1list"
    acl = "private"
    versioning {
      enabled = true
    }
    lifecycle_rule {
      enabled = true
      transition {
        storage_class = "STANDARD_IA"
        days = 45
      }
      transition {
        storage_class = "GLACIER"
        days = 95
      }
      transition {
        storage_class = "DEEP_ARCHIVE"
        days = 125
      }
      expiration {
        days =365
      }
    }
    logging {
      target_bucket = aws_s3_bucket.east1bkt.id
    }
}
resource "aws_s3_bucket" "east1bkt" {
    bucket = "est1bcket"
    acl = "log_delivery_write"
    versioning {
      enabled = true
    }
    lifecycle_rule {
      enabled = true
      transition {
        storage_class = "STANDARD_IA"
        days =  45
      }
      transition {
        storage_class = "GLACIER"
        days =  95
      }
      expiration {
        days = 365
      }
    }
}