provider "aws" {
    alias = "prov1"
    region =var.regions_list[1]
    
}
resource "aws_instance" "inst_est30" {
    ami = var.ami_checks.ami_est2
    instance_type = var.tools.inst_type 
    key_name = var.tools.keys
    availability_zone = var.az_list[2]
    tags = {
      name = var.tags_list[2]
    }
   count = 4
   network_interface {
     device_index = 0
     network_interface_id = aws_network_interface.intf2a.id
   }
   security_groups = [aws_security_group.sec_est2a.id]
}
resource "aws_instance" "inst_est40" {
    ami = var.ami_checks.ami_est2
    instance_type = var.tools.inst_type 
    key_name = var.tools.keys
    availability_zone = var.az_list[3]
    tags = {
      name = var.tags_list[3]
    }
    network_interface {
     device_index = 0
     network_interface_id = aws_network_interface.intf2b.id
   }
   security_groups = [aws_security_group.sec_est2b.id]
   count = 4
}
resource "aws_vpc" "vpceast2" {
    cidr_block = var.vpcs_list[1]
}
resource "aws_subnet" "sbnt0_est2" {
    cidr_block = var.subnets_list[2]
    vpc_id = aws_vpc.vpceast2.id
    availability_zone = var.az_list[2]
}
resource "aws_subnet" "sbnt1_est2" {
    cidr_block = var.subnets_list[3]
    vpc_id = aws_vpc.vpceast2.id
    availability_zone = var.az_list[3]
}
resource "aws_internet_gateway" "gweast2" {
    vpc_id = aws_vpc.vpceast2.id
}
resource "aws_route_table" "rtblest2" {
    vpc_id = aws_vpc.vpceast2.id 
    route  {
        cidr_block = "0.0.0.0/0"
        gateway_id =aws_internet_gateway.gweast2.id
    }
    route  {
        cidr_block ="0.0.0.0/0"
        gateway_id =aws_nat_gateway.nt10gw10.id
    }
    route  {
        cidr_block = "0.0.0.0/0"
        gateway_id =aws_nat_gateway.nt30gw_est2.id
    }
}
resource "aws_route_table_association" "assoctest2a" {
    subnet_id = aws_subnet.sbnt0_est2.id 
    route_table_id =aws_route_table.rtblest2.id
}
resource "aws_route_table_association" "assoctest2b" {
    subnet_id = aws_subnet.sbnt1_est2.id 
    route_table_id =aws_route_table.rtblest2.id
}

resource "aws_security_group" "sec_est2a" {
    name ="sec2a"
    vpc_id = aws_vpc.vpceast2.id  
    ingress  {
        from_port = 80
        to_port   = 80 
        protocol = "tcp"
        cidr_blocks = [var.subnets_list[2]]
    }
    ingress  {
        from_port = 443
        to_port   = 443
        protocol = "tcp"
        cidr_blocks = [var.subnets_list[2]]
    }
    ingress  {
        from_port = 22
        to_port   = 22 
        protocol = "tcp"
        cidr_blocks = [var.subnets_list[2]]
    }
    egress  {
        from_port = 0
        to_port   = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
  
}

resource "aws_security_group" "sec_est2b" {
    name ="sec2b"
    vpc_id = aws_vpc.vpceast2.id
    ingress  {
        from_port = 80
        to_port   = 80 
        protocol = "tcp"
        cidr_blocks = [var.subnets_list[3]]
    }
    ingress  {
        from_port = 443
        to_port   = 443
        protocol = "tcp"
        cidr_blocks = [var.subnets_list[3]]
    }
    ingress  {
        from_port = 22
        to_port   = 22 
        protocol = "tcp"
        cidr_blocks = [var.subnets_list[3]]
    }
    egress  {
        from_port = 0
        to_port   = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
}
resource "aws_network_interface" "intf2a" {
    subnet_id = aws_subnet.sbnt0_est2.id  
    private_ips = [var.ipaddr_list[2]]
    security_groups = [aws_security_group.sec_est2a.id]
}
resource "aws_network_interface" "intf2b" {
    subnet_id = aws_subnet.sbnt1_est2.id  
    private_ips = [var.ipaddr_list[3]]
    security_groups = [aws_security_group.sec_est2b.id]
}
resource "aws_eip" "eip_est2a" {
    vpc =  true 
    network_interface = aws_network_interface.intf2a.id  
    associate_with_private_ip = var.ipaddr_list[2]
    depends_on =[aws_internet_gateway.gweast2]
  
}
resource "aws_eip" "eip_est2b" {
    vpc =  true 
    network_interface = aws_network_interface.intf2b.id  
    associate_with_private_ip = var.ipaddr_list[3]
    depends_on =[aws_internet_gateway.gweast2]
  
}
resource "aws_ebs_volume" "ebs150" {
    size = 10
    availability_zone = var.az_list[2]
  
}
resource "aws_ebs_snapshot" "bkup150" {
    volume_id = aws_ebs_volume.ebs150.id
}
resource "aws_ebs_volume" "ebs151" {
    size = 10
    snapshot_id = aws_ebs_snapshot.bkup150.id
    availability_zone = var.az_list[2]
}
resource "aws_ebs_volume" "ebs152" {
    size = 10
    snapshot_id = aws_ebs_snapshot.bkup150.id
    availability_zone = var.az_list[2]
}
resource "aws_ebs_volume" "ebs153" {
    size = 10
    snapshot_id = aws_ebs_snapshot.bkup150.id
    availability_zone = var.az_list[2]
}

resource "aws_volume_attachment" "attach300" {
    device_name = "/dev/sda30"
    volume_id = aws_ebs_volume.ebs150.id 
    instance_id = aws_instance.inst_est30[0].id
}
resource "aws_volume_attachment" "attach301" {
    device_name = "/dev/sda31"
    volume_id = aws_ebs_volume.ebs151.id 
    instance_id = aws_instance.inst_est30[1].id
}
resource "aws_volume_attachment" "attach302" {
    device_name = "/dev/sda32"
    volume_id = aws_ebs_volume.ebs152.id 
    instance_id = aws_instance.inst_est30[2].id
}
resource "aws_volume_attachment" "attach303" {
    device_name = "/dev/sda33"
    volume_id = aws_ebs_volume.ebs153.id 
    instance_id = aws_instance.inst_est30[3].id
}

resource "aws_ebs_volume" "ebs160" {
    size = 10
    availability_zone = var.az_list[3]
  
}
resource "aws_ebs_snapshot" "bkup160" {
    volume_id = aws_ebs_volume.ebs160.id
}
resource "aws_ebs_volume" "ebs161" {
    size = 10
    snapshot_id = aws_ebs_snapshot.bkup160.id
    availability_zone = var.az_list[3]
}
resource "aws_ebs_volume" "ebs162" {
    size = 10
    snapshot_id = aws_ebs_snapshot.bkup160.id
    availability_zone = var.az_list[3]
}
resource "aws_ebs_volume" "ebs163" {
    size = 10
    snapshot_id = aws_ebs_snapshot.bkup160.id
    availability_zone = var.az_list[3]
}

resource "aws_volume_attachment" "attach400" {
    device_name = "/dev/sda40"
    volume_id = aws_ebs_volume.ebs160.id 
    instance_id = aws_instance.inst_est40[0].id
}
resource "aws_volume_attachment" "attach401" {
    device_name = "/dev/sda41"
    volume_id = aws_ebs_volume.ebs161.id 
    instance_id = aws_instance.inst_est40[1].id
}
resource "aws_volume_attachment" "attach402" {
    device_name = "/dev/sda42"
    volume_id = aws_ebs_volume.ebs162.id 
    instance_id = aws_instance.inst_est40[2].id
}
resource "aws_volume_attachment" "attach403" {
    device_name = "/dev/sda43"
    volume_id = aws_ebs_volume.ebs163.id 
    instance_id = aws_instance.inst_est40[3].id
}

#user-data =<<EOF
#!bin/bash
#sudo yum update -y
#sudo yum upgrade -y
#sudo yum install apache2 -y
#sudo systemctl start httpd  echo "c welcome to first webserver_east1"c /var/www/index.html
#EOf

