provider "aws" {
    alias = "est1_lb"
    region =var.regions_list[0]
    
}
# create  load balancer  type  network for us-east-1
resource "aws_lb" "east1_lb" {
    name = "est1lb"
    load_balancer_type = "network"
    enable_cross_zone_load_balancing = true
    enable_deletion_protection = true
    subnet_mapping {
      allocation_id = aws_eip.eip_est1a.id 
      subnet_id = aws_subnet.sbnt0_est1.id
    }
    subnet_mapping {
      allocation_id = aws_eip.eip_est1b.id 
      subnet_id = aws_subnet.sbnt1_est1.id
    }
    idle_timeout =  30
    security_groups = [aws_security_group.sc_east1lb.id]
    
}
resource "aws_security_group" "sc_east1lb" {
    name = "sest1lb"
    vpc_id = aws_vpc.vpcest1.id
      ingress  {
      from_port = 80
      to_port   = 80
      protocol ="tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }
    ingress  {
      from_port = 443
      to_port   = 443
      protocol="tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }
    ingress  {
      from_port = 22
      to_port   = 22
      protocol ="tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }
    egress  {
      from_port = 0
      to_port   = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
  }
}
resource "aws_lb_listener" "lis_est1" {
    load_balancer_arn = aws_lb.east1_lb.arn 
    port = 80
    protocol = "tcp"
    default_action {
      type = "forward"
      target_group_arn = aws_lb_target_group.trgt_est1.arn
    }
  
}
resource "aws_lb_target_group" "trgt_est1" {
  load_balancing_algorithm_type = "round_robin"
  protocol = "tcp"
  port = 80
  target_type = "instance"
 health_check {
    path = aws_s3_bucket.east1bkt.id
    timeout = 30
  }
  vpc_id = aws_vpc.vpcest1.id
}
resource "aws_lb_listener" "lis_est2" {
    load_balancer_arn = aws_lb.east1_lb.arn 
    port = 443
    protocol = "tcp"
    default_action {
      type = "forward"
      target_group_arn = aws_lb_target_group.trgt_est2.arn
    }
  
}
resource "aws_lb_target_group" "trgt_est2" {
  load_balancing_algorithm_type = "round_robin"
  protocol = "tcp"
  port = 443
  target_type = "instance"
   health_check {
    path = aws_s3_bucket.east1bkt.id
    timeout = 30
  }
  vpc_id = aws_vpc.vpcest1.id
}
resource "aws_lb_listener" "lis_est3" {
    load_balancer_arn = aws_lb.east1_lb.arn 
    port = 22
    protocol = "tcp"
    default_action {
      type = "forward"
      target_group_arn = aws_lb_target_group.trgt_est3.arn
    }
  
}
resource "aws_lb_target_group" "trgt_est3" {
  load_balancing_algorithm_type = "least_outstanding_requests"
  protocol = "tcp"
  port = 22
  target_type = "instance"
   health_check {
    path = aws_s3_bucket.east1bkt.id
    timeout = 30
  }
  vpc_id = aws_vpc.vpcest1.id
}