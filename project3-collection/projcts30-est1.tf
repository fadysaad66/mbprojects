provider "aws" {
    region =var.regions_list[0]
}
resource "aws_instance" "inst_east10" {
    ami = var.ami_checks.ami_est1
    instance_type =var.tools.inst_type 
    key_name =var.tools.keys
    count = 4
    tags = {
    "name" = var.tags_list[0]
  }
  network_interface {
    device_index = 0
    network_interface_id = aws_network_interface.intf1a.id
  }
  security_groups = [aws_security_group.sec_est1a.id]
}
resource "aws_instance" "inst_east20" {
  ami = var.ami_checks.ami_est1
  instance_type = var.tools.inst_type
  key_name = var.tools.keys
  count = 4
  tags = {
    "name" = var.tags_list[1]
  }
  network_interface {
    device_index = 0
    network_interface_id = aws_network_interface.intf1b.id
  }
  security_groups = [aws_security_group.sec_est1b.id]
}
resource "aws_vpc" "vpcest1" {
    cidr_block =var.vpcs_list[0]
}
resource "aws_subnet" "sbnt0_est1" {
    cidr_block =var.subnets_list[0]
    vpc_id =aws_vpc.vpcest1.id
    availability_zone = var.az_list[0]
}
resource "aws_subnet" "sbnt1_est1" {
    cidr_block =var.subnets_list[1]
    vpc_id =aws_vpc.vpcest1.id
    availability_zone = var.az_list[1]
}
resource "aws_internet_gateway" "gweast1" {
vpc_id = aws_vpc.vpcest1.id

}
resource "aws_route_table" "rtblest1" {
    vpc_id = aws_vpc.vpcest1.id
     route {
         cidr_block = "0.0.0.0/0"
         gateway_id = aws_internet_gateway.gweast1.id
    
     }
    route  {
         cidr_block  = "0.0.0.0/0"
         gateway_id =aws_nat_gateway.nt1gw1.id
    }
    route {
        cidr_block  = "0.0.0.0/0"
        gateway_id  =aws_nat_gateway.nt2gw2.id
    }

  
}
resource "aws_route_table_association" "asscte1a" {
    subnet_id = aws_subnet.sbnt0_est1.id  
    route_table_id =aws_route_table.rtblest1.id
  
}
resource "aws_route_table_association" "asscte1b" {
    subnet_id = aws_subnet.sbnt1_est1.id  
    route_table_id =aws_route_table.rtblest1.id
}
resource "aws_security_group" "sec_est1a" {
    name ="sec1a"
    vpc_id = aws_vpc.vpcest1.id  
    ingress  {
        from_port = 80
        to_port   = 80 
        protocol = "tcp"
        cidr_blocks = [var.subnets_list[0]]
    }
    ingress  {
        from_port = 443
        to_port   = 443
        protocol = "tcp"
        cidr_blocks = [var.subnets_list[0]]
    }
    ingress  {
        from_port = 22
        to_port   = 22 
        protocol = "tcp"
        cidr_blocks = [var.subnets_list[0]]
    }
    egress  {
        from_port = 0
        to_port   = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
  
}

resource "aws_security_group" "sec_est1b" {
    name ="sec1b"
    vpc_id = aws_vpc.vpcest1.id
    ingress  {
        from_port = 80
        to_port   = 80 
        protocol = "tcp"
        cidr_blocks = [var.subnets_list[1]]
    }
    ingress  {
        from_port = 443
        to_port   = 443
        protocol = "tcp"
        cidr_blocks = [var.subnets_list[1]]
    }
    ingress  {
        from_port = 22
        to_port   = 22 
        protocol = "tcp"
        cidr_blocks = [var.subnets_list[1]]
    }
    egress  {
        from_port = 0
        to_port   = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
  
}
resource "aws_network_interface" "intf1a" {
    subnet_id = aws_subnet.sbnt0_est1.id  
    private_ips = [var.ipaddr_list[0]]
    security_groups = [aws_security_group.sec_est1a.id]
}
resource "aws_network_interface" "intf1b" {
    subnet_id = aws_subnet.sbnt1_est1.id  
    private_ips = [var.ipaddr_list[1]]
    security_groups = [aws_security_group.sec_est1b.id]
}
resource "aws_eip" "eip_est1a" {
    vpc =  true 
    network_interface = aws_network_interface.intf1a.id  
    associate_with_private_ip = var.ipaddr_list[0]
    depends_on =[aws_internet_gateway.gweast1]
  
}
resource "aws_eip" "eip_est1b" {
    vpc =  true 
    network_interface = aws_network_interface.intf1b.id  
    associate_with_private_ip = var.ipaddr_list[1]
    depends_on =[aws_internet_gateway.gweast1]
  
}
resource "aws_ebs_volume" "ebs1" {
    size = 10
    availability_zone = var.az_list[0]
}
resource "aws_volume_attachment" "attach1" {
    device_name = "/dev/sda1"
    volume_id = aws_ebs_volume.ebs1.id 
    instance_id = aws_instance.inst_east10[0].id
}
resource "aws_ebs_snapshot" "bkup" {
    volume_id = aws_ebs_volume.ebs1.id 
}
resource "aws_ebs_volume" "ebs2" {
    size = 10
    snapshot_id = aws_ebs_snapshot.bkup.id
    availability_zone = var.az_list[0]
}
resource "aws_ebs_volume" "ebs3" {
    size = 10
    snapshot_id = aws_ebs_snapshot.bkup.id
    availability_zone = var.az_list[0]
}
resource "aws_ebs_volume" "ebs4" {
    size = 10
    snapshot_id = aws_ebs_snapshot.bkup.id
    availability_zone = var.az_list[0]
}
resource "aws_volume_attachment" "attach2" {
    device_name = "/dev/sda2"
    volume_id = aws_ebs_volume.ebs2.id 
    instance_id = aws_instance.inst_east10[1].id
}
resource "aws_volume_attachment" "attach3" {
    device_name = "/dev/sda3"
    volume_id = aws_ebs_volume.ebs3.id 
    instance_id = aws_instance.inst_east10[2].id
}
resource "aws_volume_attachment" "attach4" {
    device_name = "/dev/sda4"
    volume_id = aws_ebs_volume.ebs4.id 
    instance_id = aws_instance.inst_east10[3].id
}
resource "aws_ebs_volume" "ebs20" {
    size = 10
    availability_zone = var.az_list[1]
}
resource "aws_ebs_snapshot" "bkup20" {
 volume_id =aws_ebs_volume.ebs20.id
}
resource "aws_ebs_volume" "ebs200" {
    size = 10
    snapshot_id = aws_ebs_snapshot.bkup20.id
    availability_zone = var.az_list[1]
}
resource "aws_ebs_volume" "ebs201" {
    size = 10
    snapshot_id = aws_ebs_snapshot.bkup20.id
    availability_zone = var.az_list[1]
}
resource "aws_ebs_volume" "ebs202" {
    size = 10
    snapshot_id = aws_ebs_snapshot.bkup20.id
    availability_zone = var.az_list[1]
}
resource "aws_volume_attachment" "attach20" {
    device_name = "dev/sda5"
    volume_id = aws_ebs_volume.ebs20.id 
    instance_id = aws_instance.inst_east20[0].id
}
resource "aws_volume_attachment" "attach21" {
    device_name = "dev/sda6"
    volume_id = aws_ebs_volume.ebs200.id 
    instance_id = aws_instance.inst_east20[1].id

}
resource "aws_volume_attachment" "attach22" {
    device_name = "dev/sda7"
    volume_id = aws_ebs_volume.ebs201.id 
    instance_id = aws_instance.inst_east20[2].id

}
resource "aws_volume_attachment" "attach23" {
    device_name = "dev/sda8"
    volume_id = aws_ebs_volume.ebs202.id 
    instance_id = aws_instance.inst_east20[3].id
}

locals {
    env = terraform.workspace
}


#user-data =<<EOF
#!bin/bash
#sudo yum update -y
#sudo yum upgrade -y
#sudo yum install apache2 -y
#sudo systemctl start httpd  echo "c welcome to first webserver_east1"c /var/www/index.html
#EOf



    
    