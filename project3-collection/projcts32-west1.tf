provider "aws" {
    alias = "prov2"
    region =var.regions_list[2]
    
}
resource "aws_instance" "inst_west50" {
    ami = var.ami_checks.ami_wst1
    instance_type = var.tools.inst_type 
    key_name = var.tools.keys
    availability_zone = var.az_list[4]
    tags = {
      name = var.tags_list[4]
    }
    network_interface {
      device_index = 0
      network_interface_id = aws_network_interface.intf0_wst2a.id
    }
    security_groups = [aws_security_group.sec_wst2a.id]
   count = 4
}
resource "aws_instance" "inst_west60" {
    ami = var.ami_checks.ami_wst1
    instance_type = var.tools.inst_type 
    key_name = var.tools.keys
    availability_zone = var.az_list[5]
    tags = {
      name = var.tags_list[5]
    }
    network_interface {
      device_index = 0
      network_interface_id = aws_network_interface.intf1_wst2b.id
    }
    security_groups = [aws_security_group.sec_wst2b.id]
   count = 4
}
resource "aws_vpc" "vpcwst1" {
    cidr_block = var.vpcs_list[2]
  
}
resource "aws_subnet" "sbnt0_west1" {
    cidr_block = var.subnets_list[4]
    vpc_id = aws_vpc.vpcwst1.id 
    availability_zone = var.az_list[4]
}
resource "aws_subnet" "sbnt1_west1" {
    cidr_block = var.subnets_list[5]
    vpc_id = aws_vpc.vpcwst1.id 
    availability_zone = var.az_list[5]
}
resource "aws_internet_gateway" "gwest1" {
  vpc_id = aws_vpc.vpcwst1.id
}
resource "aws_route_table" "rtblwest1" {
    vpc_id = aws_vpc.vpcwst1.id 
    route  {
        cidr_block ="0.0.0.0/0"
        gateway_id  =aws_internet_gateway.gwest1.id
    }
    route   {
        cidr_block = "0.0.0.0/0"
         gateway_id = aws_nat_gateway.nt25gw25.id
    }
    route   {
        cidr_block = "0.0.0.0/0"
        gateway_id =aws_nat_gateway.nt31gw_wst1.id
    }
}
resource "aws_route_table_association" "assctewst1" {
    subnet_id = aws_subnet.sbnt0_west1.id 
    route_table_id = aws_route_table.rtblwest1.id
}
resource "aws_route_table_association" "assctewst2" {
    subnet_id = aws_subnet.sbnt1_west1.id 
    route_table_id = aws_route_table.rtblwest1.id
}
resource "aws_security_group" "sec_wst2a" {
    name ="sec-wst2a"
    vpc_id = aws_vpc.vpcwst1.id
    ingress  {
        from_port = 80
        to_port   = 80 
        protocol = "tcp"
        cidr_blocks = [var.subnets_list[4]]
    }
    ingress  {
        from_port = 443
        to_port   = 443
        protocol = "tcp"
        cidr_blocks = [var.subnets_list[4]]
    }
    ingress  {
        from_port = 22
        to_port   = 22 
        protocol = "tcp"
        cidr_blocks = [var.subnets_list[4]]
    }
    egress  {
        from_port = 0
        to_port   = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
  
}

resource "aws_security_group" "sec_wst2b" {
    name ="sec-wst2b"
    vpc_id = aws_vpc.vpcwst1.id
    ingress  {
        from_port = 80
        to_port   = 80 
        protocol = "tcp"
        cidr_blocks = [var.subnets_list[5]]
    }
    ingress  {
        from_port = 443
        to_port   = 443
        protocol = "tcp"
        cidr_blocks = [var.subnets_list[5]]
    }
    ingress  {
        from_port = 22
        to_port   = 22 
        protocol = "tcp"
        cidr_blocks = [var.subnets_list[5]]
    }
    egress  {
        from_port = 0
        to_port   = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
}
resource "aws_network_interface" "intf0_wst2a" {
    subnet_id = aws_subnet.sbnt0_west1.id  
    private_ips = [var.ipaddr_list[4]]
    security_groups = [aws_security_group.sec_wst2a.id]
}
resource "aws_network_interface" "intf1_wst2b" {
    subnet_id = aws_subnet.sbnt1_west1.id  
    private_ips = [var.ipaddr_list[5]]
    security_groups = [aws_security_group.sec_wst2b.id]
}
resource "aws_eip" "eip_wst1a" {
    vpc =  true 
    network_interface = aws_network_interface.intf0_wst2a.id  
    associate_with_private_ip = var.ipaddr_list[4]
    depends_on =[aws_internet_gateway.gwest1]
  
}
resource "aws_eip" "eip_wst1b" {
    vpc =  true 
    network_interface = aws_network_interface.intf1_wst2b.id  
    associate_with_private_ip = var.ipaddr_list[5]
    depends_on =[aws_internet_gateway.gwest1]
  
}

resource "aws_ebs_volume" "ebs170" {
    size = 10
    availability_zone = var.az_list[4]
  
}

resource "aws_ebs_volume" "ebs171" {
    size = 10
    snapshot_id = aws_ebs_snapshot.bkup170.id
    availability_zone = var.az_list[4]
  
}
resource "aws_ebs_volume" "ebs172" {
    size = 10
    snapshot_id = aws_ebs_snapshot.bkup170.id
    availability_zone = var.az_list[4]
  
}
resource "aws_ebs_volume" "ebs173" {
    size = 10
    snapshot_id = aws_ebs_snapshot.bkup170.id
    availability_zone = var.az_list[4]
  
}
resource "aws_ebs_snapshot" "bkup170" {
    volume_id = aws_ebs_volume.ebs170.id
}
resource "aws_ebs_snapshot" "bkup180" {
    volume_id = aws_ebs_volume.ebs180.id
}
resource "aws_ebs_volume" "ebs180" {
    size = 10
    availability_zone = var.az_list[5]
}
resource "aws_ebs_volume" "ebs181" {
    size = 10
    snapshot_id = aws_ebs_snapshot.bkup180.id
    availability_zone = var.az_list[5]
}
resource "aws_ebs_volume" "ebs182" {
    size = 10
    snapshot_id = aws_ebs_snapshot.bkup180.id
    availability_zone = var.az_list[5]
}
resource "aws_ebs_volume" "ebs183" {
    size = 10
    snapshot_id = aws_ebs_snapshot.bkup180.id
    availability_zone = var.az_list[5]
}


resource "aws_volume_attachment" "attach500" {
    device_name = "dev/sda80"
    volume_id = aws_ebs_volume.ebs170.id
    instance_id = aws_instance.inst_west50[0].id
}
resource "aws_volume_attachment" "attach501" {
    device_name = "dev/sda81"
    volume_id = aws_ebs_volume.ebs171.id
    instance_id = aws_instance.inst_west50[1].id
}
resource "aws_volume_attachment" "attach502" {
    device_name = "dev/sda82"
    volume_id = aws_ebs_volume.ebs172.id
    instance_id = aws_instance.inst_west50[2].id
}
resource "aws_volume_attachment" "attach503" {
    device_name = "dev/sda83"
    volume_id = aws_ebs_volume.ebs173.id
    instance_id = aws_instance.inst_west50[3].id
}
resource "aws_volume_attachment" "attach600" {
    device_name = "dev/sda90"
    volume_id = aws_ebs_volume.ebs180.id
    instance_id = aws_instance.inst_west50[0].id
}
resource "aws_volume_attachment" "attach601" {
    device_name = "dev/sda91"
    volume_id = aws_ebs_volume.ebs181.id
    instance_id = aws_instance.inst_west60[1].id
}
resource "aws_volume_attachment" "attach602" {
    device_name = "dev/sda92"
    volume_id = aws_ebs_volume.ebs182.id
    instance_id = aws_instance.inst_west60[2].id
}
resource "aws_volume_attachment" "attach603" {
    device_name = "dev/sda93"
    volume_id = aws_ebs_volume.ebs183.id
    instance_id = aws_instance.inst_west60[3].id
}



#user-data =<<EOF
#!bin/bash
#sudo yum update -y
#sudo yum upgrade -y
#sudo yum install apache2 -y
#sudo systemctl start httpd  echo "c welcome to first webserver_east1"c /var/www/index.html
#EOf