provider "aws" {
    alias = "west1_prov"
    region =var.regions_list[2]
    
}
resource "aws_vpc" "vpc_prng20" { 
    cidr_block = var.vpcs_prng[1]
}
resource "aws_subnet" "sbnt_prng20" {
    cidr_block = var.vpcs_prng[3]
    vpc_id = aws_vpc.vpc_prng20.id
}
resource "aws_internet_gateway" "gwprng20" {
    vpc_id = aws_vpc.vpc_prng20.id
}
resource "aws_route_table" "rtble_prng20" {
    vpc_id = aws_vpc.vpc_prng20.id
    route  {
        cidr_block = "0.0.0.0/0"
        gateway_id =aws_internet_gateway.gwprng10.id
    }
  
}
resource "aws_route_table_association" "assct_prng201" {
    subnet_id = aws_subnet.sbnt_prng10.id  
    route_table_id = aws_route_table.rtble_prng10.id
}
resource "aws_security_group" "sec_prng201" {
    name = "scprng201"
    vpc_id = aws_vpc.vpc_prng10.id
    ingress  {
        from_port = 80
        to_port   = 80
        protocol  = "tcp"
        cidr_blocks = [var.vpcs_prng[3]]
    }
    ingress  {
        from_port = 443
        to_port   = 443
        protocol  = "tcp"
        cidr_blocks = [var.vpcs_prng[3]]
    }
    ingress  {
        from_port = 22
        to_port   = 22
        protocol  = "tcp"
        cidr_blocks = [var.vpcs_prng[3]]
    }
    egress  {
        from_port = 0
        to_port   = 0
        protocol  = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
}
resource "aws_network_interface" "intf20_prng20" {
    subnet_id = aws_subnet.sbnt_prng10.id  
    private_ips = [var.vpcs_prng[7]]
    security_groups = [aws_security_group.sec_prng201.id]
}
resource "aws_eip" "eip_prng201" {
    vpc = true
    network_interface = aws_network_interface.intf20_prng20.id  
    associate_with_private_ip = var.vpcs_prng[7]
    depends_on =[aws_internet_gateway.gwprng10]
}
# vpc peering  between  us-west-1 and us-east-1
resource "aws_vpc_peering_connection" "connect_vpc20" {
peer_vpc_id = aws_vpc.vpc_prng1.id
peer_region = var.regions_list[0]
vpc_id = aws_vpc.vpc_prng20.id
    accepter {
      allow_remote_vpc_dns_resolution = true
    }
    requester {
      allow_remote_vpc_dns_resolution = true
    }
}
resource "aws_nat_gateway" "nt25gw25" {
    allocation_id = aws_eip.eip_prng201.id  
    subnet_id = aws_subnet.sbnt_prng20.id
  
}