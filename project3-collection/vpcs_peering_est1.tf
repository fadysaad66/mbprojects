provider "aws" {
    alias = "est1_prov"
    region =var.regions_list[0]
    
}
# create  2 vpc  for  peering connection  with us-east-2 and us-west-1  region
resource "aws_vpc" "vpc_prng1" { 
    cidr_block = var.vpcs_prng[0]
}
resource "aws_subnet" "sbnt_prng1" {
    cidr_block = var.vpcs_prng[2]
    vpc_id = aws_vpc.vpc_prng1.id
}
resource "aws_vpc" "vpc_prng2" { 
    cidr_block = var.vpcs_prng[1]
}
resource "aws_subnet" "sbnt_prng2" {
    cidr_block = var.vpcs_prng[3]
    vpc_id = aws_vpc.vpc_prng2.id
}
resource "aws_internet_gateway" "gwprng1" {
    vpc_id = aws_vpc.vpc_prng1.id
}
resource "aws_internet_gateway" "gwprng2" {
    vpc_id = aws_vpc.vpc_prng2.id
}
resource "aws_route_table" "rtble_prng1" {
    vpc_id = aws_vpc.vpc_prng1.id
    route  {
        cidr_block = "0.0.0.0/0"
        gateway_id =aws_internet_gateway.gwprng1.id
    }
  
}
resource "aws_route_table" "rtble_prng2" {
    vpc_id = aws_vpc.vpc_prng2.id
    route  {
        cidr_block = "0.0.0.0/0"
        gateway_id =aws_internet_gateway.gwprng2.id
        
    }
  
}
resource "aws_route_table_association" "assct_prng1" {
    subnet_id = aws_subnet.sbnt_prng1.id  
    route_table_id = aws_route_table.rtble_prng1.id
}
resource "aws_route_table_association" "assct_prng2" {
    subnet_id = aws_subnet.sbnt_prng2.id  
    route_table_id = aws_route_table.rtble_prng2.id
}

resource "aws_security_group" "sec_prng1" {
    name = "scprng1"
    vpc_id = aws_vpc.vpc_prng1.id
    ingress  {
        from_port = 80
        to_port   = 80
        protocol  = "tcp"
        cidr_blocks = [var.vpcs_prng[2]]
    }
    ingress  {
        from_port = 443
        to_port   = 443
        protocol  = "tcp"
        cidr_blocks = [var.vpcs_prng[2]]
    }
    ingress  {
        from_port = 22
        to_port   = 22
        protocol  = "tcp"
        cidr_blocks = [var.vpcs_prng[2]]
    }
    egress  {
        from_port = 0
        to_port   = 0
        protocol  = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
}

resource "aws_security_group" "sec_prng2" {
    name = "scprng2"
    vpc_id = aws_vpc.vpc_prng2.id
    ingress  {
        from_port = 80
        to_port   = 80
        protocol  = "tcp"
        cidr_blocks = [var.vpcs_prng[3]]
    }
    ingress  {
        from_port = 443
        to_port   = 443
        protocol  = "tcp"
        cidr_blocks = [var.vpcs_prng[3]]
    }
    ingress  {
        from_port = 22
        to_port   = 22
        protocol  = "tcp"
        cidr_blocks = [var.vpcs_prng[3]]
    }
    egress  {
        from_port = 0
        to_port   = 0
        protocol  = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
}
resource "aws_network_interface" "intf0_prng1" {
    subnet_id = aws_subnet.sbnt_prng1.id
    private_ips = [var.vpcs_prng[4]]
    security_groups = [aws_security_group.sec_prng1.id]
}
resource "aws_network_interface" "intf1_prng2" {
    subnet_id = aws_subnet.sbnt_prng2.id  
    private_ips = [var.vpcs_prng[5]]
    security_groups = [aws_security_group.sec_prng2.id]
}
resource "aws_eip" "eip_prng100" {
    vpc = true
    network_interface = aws_network_interface.intf0_prng1.id  
    associate_with_private_ip = var.vpcs_prng[4]
    depends_on =[aws_internet_gateway.gwprng1]
}
resource "aws_eip" "eip_prng200" {
    vpc = true
    network_interface = aws_network_interface.intf1_prng2.id  
    associate_with_private_ip = var.vpcs_prng[5]
    depends_on =[aws_internet_gateway.gwprng2]
}
# vpc peering  between  us-east-1 and us-east-2
resource "aws_vpc_peering_connection" "connect_vpc1" {
peer_vpc_id = aws_vpc.vpc_prng10.id
peer_region = var.regions_list[1]
vpc_id = aws_vpc.vpc_prng1.id
    accepter {
      allow_remote_vpc_dns_resolution = true
    }
    requester {
      allow_remote_vpc_dns_resolution = true
    }
}
 # vpc peering  between  us-east-1 and us-west-1
resource "aws_vpc_peering_connection" "connect_vpc2" {
peer_vpc_id = aws_vpc.vpc_prng20.id
peer_region = var.regions_list[2]
vpc_id = aws_vpc.vpc_prng2.id
    accepter {
      allow_remote_vpc_dns_resolution = true
    }
    requester {
      allow_remote_vpc_dns_resolution = true
    }
}
resource "aws_nat_gateway" "nt1gw1" {
    allocation_id = aws_eip.eip_prng100.id
    subnet_id = aws_subnet.sbnt_prng1.id
}
resource "aws_nat_gateway" "nt2gw2" {
    allocation_id = aws_eip.eip_prng200.id 
    subnet_id =  aws_subnet.sbnt_prng2.id
  
}

