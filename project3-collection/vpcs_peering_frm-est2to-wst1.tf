provider "aws" {
    alias = "est2_to_wst1_peering"
    region =var.regions_list[1]
    
}

#  peering connection  to  us-west-1 
resource "aws_vpc" "vpcnt1" {
    cidr_block = var.vpcs_list[0]
}
resource "aws_subnet" "sbnt_to_wst1" {
    cidr_block = var.vpcs_list[1]
    vpc_id = aws_vpc.vpcnt1.id
}
resource "aws_internet_gateway" "gwprg_est2" {
    vpc_id = aws_vpc.vpcnt1.id
}
resource "aws_route_table" "rtest2_to_wst1" {
    vpc_id = aws_vpc.vpcnt1.id 
    route  {
        cidr_block ="0.0.0.0/0"
        gateway_id  =aws_internet_gateway.gwprg_est2.id
    }
  
}
resource "aws_route_table_association" "est2-to-wst1" {
    subnet_id = aws_subnet.sbnt_to_wst1.id  
    route_table_id = aws_route_table.rtest2_to_wst1.id
}
resource "aws_security_group" "sec_prng250" {
    name = "scprng251"
    vpc_id = aws_vpc.vpcnt1.id
    ingress  {
        from_port = 80
        to_port   = 80
        protocol  = "tcp"
        cidr_blocks = [var.vpcs_prng[1]]
    }
    ingress  {
        from_port = 443
        to_port   = 443
        protocol  = "tcp"
        cidr_blocks = [var.vpcs_prng[1]]
    }
    ingress  {
        from_port = 22
        to_port   = 22
        protocol  = "tcp"
        cidr_blocks = [var.vpcs_prng[1]]
    }
    egress  {
        from_port = 0
        to_port   = 0
        protocol  = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
}
resource "aws_network_interface" "intf_est2to_wst1" {
    subnet_id = aws_subnet.sbnt_to_wst1.id 
    private_ips = [var.vpcs_prng[2]]
    security_groups = [aws_security_group.sec_prng250.id]
}
resource "aws_eip" "eip_est2to_wst1" {
    vpc = true 
    network_interface = aws_network_interface.intf_est2to_wst1.id 
    associate_with_private_ip = var.vpcs_prng[2]
    depends_on  = [aws_internet_gateway.gwprg_est2]
}
resource "aws_nat_gateway" "nt30gw_est2" {
    allocation_id = aws_eip.eip_est2to_wst1.id 
    subnet_id = aws_subnet.sbnt_to_wst1.id
}
