provider "aws" {
    alias = "wst1_to_est2_peering"
    region =var.regions_list[2]
    
}

#  peering connection  to  us-east-2
resource "aws_vpc" "vpcnt2" {
    cidr_block = var.vpcs_list[0]
}
resource "aws_subnet" "sbnt_to_est2" {
    cidr_block = var.vpcs_list[1]
    vpc_id = aws_vpc.vpcnt2.id
}
resource "aws_internet_gateway" "gwprg1_wst1" {
    vpc_id = aws_vpc.vpcnt2.id
}
resource "aws_route_table" "rtest2_to_est2" {
    vpc_id = aws_vpc.vpcnt2.id 
    route  {
        cidr_block ="0.0.0.0/0"
        gateway_id  =aws_internet_gateway.gwprg1_wst1.id
    }
  
}
resource "aws_route_table_association" "wst1-to-est2" {
    subnet_id = aws_subnet.sbnt_to_est2.id  
    route_table_id = aws_route_table.rtest2_to_est2.id
}
resource "aws_security_group" "sec_prng260" {
    name = "scprng261"
    vpc_id = aws_vpc.vpcnt2.id
    ingress  {
        from_port = 80
        to_port   = 80
        protocol  = "tcp"
        cidr_blocks = [var.vpcs_prng[1]]
    }
    ingress  {
        from_port = 443
        to_port   = 443
        protocol  = "tcp"
        cidr_blocks = [var.vpcs_prng[1]]
    }
    ingress  {
        from_port = 22
        to_port   = 22
        protocol  = "tcp"
        cidr_blocks = [var.vpcs_prng[1]]
    }
    egress  {
        from_port = 0
        to_port   = 0
        protocol  = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
}
resource "aws_network_interface" "intf_wst1to_est2" {
    subnet_id = aws_subnet.sbnt_to_est2.id
    private_ips = [var.vpcs_prng[3]]
    security_groups = [aws_security_group.sec_prng260.id]
}
resource "aws_eip" "eip_wst1_toest2" {
    vpc = true 
    network_interface = aws_network_interface.intf_wst1to_est2.id
    associate_with_private_ip = var.vpcs_prng[3]
    depends_on  = [aws_internet_gateway.gwprg1_wst1]
}
# nat  gateway  to   us-east-2 
resource "aws_nat_gateway" "nt31gw_wst1" {
    allocation_id = aws_eip.eip_wst1_toest2.id
    subnet_id = aws_subnet.sbnt_to_est2.id
}
