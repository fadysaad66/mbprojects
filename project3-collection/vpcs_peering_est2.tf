provider "aws" {
    alias = "est2_prov"
    region =var.regions_list[1]
    
}
resource "aws_vpc" "vpc_prng10" { 
    cidr_block = var.vpcs_prng[0]
}
resource "aws_subnet" "sbnt_prng10" {
    cidr_block = var.vpcs_prng[2]
    vpc_id = aws_vpc.vpc_prng10.id
}
resource "aws_internet_gateway" "gwprng10" {
    vpc_id = aws_vpc.vpc_prng10.id
}
resource "aws_route_table" "rtble_prng10" {
    vpc_id = aws_vpc.vpc_prng10.id
    route  {
        cidr_block = "0.0.0.0/0"
        gateway_id =aws_internet_gateway.gwprng10.id
    }
  
}
resource "aws_route_table_association" "assct_prng101" {
    subnet_id = aws_subnet.sbnt_prng10.id  
    route_table_id = aws_route_table.rtble_prng10.id
}
resource "aws_security_group" "sec_prng101" {
    name = "scprng101"
    vpc_id = aws_vpc.vpc_prng10.id
    ingress  {
        from_port = 80
        to_port   = 80
        protocol  = "tcp"
        cidr_blocks = [var.vpcs_prng[2]]
    }
    ingress  {
        from_port = 443
        to_port   = 443
        protocol  = "tcp"
        cidr_blocks = [var.vpcs_prng[2]]
    }
    ingress  {
        from_port = 22
        to_port   = 22
        protocol  = "tcp"
        cidr_blocks = [var.vpcs_prng[2]]
    }
    egress  {
        from_port = 0
        to_port   = 0
        protocol  = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
}
resource "aws_network_interface" "intf10_prng10" {
    subnet_id = aws_subnet.sbnt_prng10.id  
    private_ips = [var.vpcs_prng[6]]
    security_groups = [aws_security_group.sec_prng101.id]
}
resource "aws_eip" "eip_prng101" {
    vpc = true
    network_interface = aws_network_interface.intf10_prng10.id  
    associate_with_private_ip = var.vpcs_prng[6]
    depends_on =[aws_internet_gateway.gwprng10]
}
# vpc peering  between  us-east-2 and us-east-1
resource "aws_vpc_peering_connection" "connect_vpc10" {
peer_vpc_id = aws_vpc.vpc_prng1.id
peer_region = var.regions_list[0]
vpc_id = aws_vpc.vpc_prng10.id
    accepter {
      allow_remote_vpc_dns_resolution = true
    }
    requester {
      allow_remote_vpc_dns_resolution = true
    }
}
resource "aws_nat_gateway" "nt10gw10" {
    allocation_id = aws_eip.eip_prng101.id 
    subnet_id = aws_subnet.sbnt_prng10.id
  
}