provider "aws" {
    region = "us-east-1"

}
# create  ec2 instances for each zone [ us-east-1a , us-east-1b , us-east-1c , us-east-1d ]

resource "aws_instance" "east1a_inst1" {
    ami = var.tools.ami_list
    instance_type = var.tools.type_list
    key_name =var.tools.keys
    availability_zone =var.zone_lists[0]
    count = 4 
    tags = { 
        name = var.names_list[0]
    }
}
resource "aws_instance" "east1b_inst1" {
    ami = var.tools.ami_list
    instance_type = var.tools.type_list
    key_name =var.tools.keys
    availability_zone =var.zone_lists[1]
    count = 4 
    tags = { 
        name = var.names_list[1]
    }
}
resource "aws_instance" "east1c_inst1" {
    ami = var.tools.ami_list
    instance_type = var.tools.type_list
    key_name =var.tools.keys
    availability_zone =var.zone_lists[2]
    count = 4 
    tags = { 
        name = var.names_list[2]
    }
}
resource "aws_instance" "east1d_inst1" {
    ami = var.tools.ami_list
    instance_type = var.tools.type_list
    key_name =var.tools.keys
    availability_zone =var.zone_lists[3]
    count = 4 
    tags = { 
        name = var.names_list[3]
    }
}
# create  vpc    
resource "aws_vpc" "vpc_east1" {
    cidr_block  = var.vpcs_list
}
# create   subnets for  each zone  
resource "aws_subnet" "subnt1a" {
    vpc_id =aws_vpc.vpc_east1.id 
    cidr_block =var.subnets_list[0]
    availability_zone =var.zone_lists[0]
}
resource "aws_subnet" "subnt1b" {
    vpc_id =aws_vpc.vpc_east1.id 
    cidr_block =var.subnets_list[1]
    availability_zone =var.zone_lists[1]
}
resource "aws_subnet" "subnt1c" {
    vpc_id =aws_vpc.vpc_east1.id 
    cidr_block =var.subnets_list[2]
    availability_zone =var.zone_lists[2]
}
resource "aws_subnet" "subnt1d" {
    vpc_id =aws_vpc.vpc_east1.id 
    cidr_block =var.subnets_list[3]
    availability_zone =var.zone_lists[3]
}
# create  internet  gateway  
resource "aws_internet_gateway" "gweast1" {
    vpc_id=aws_vpc.vpc_east1.id
}

# create  route table

resource "aws_route_table" "rteast1" {
    vpc_id= aws_vpc.vpc_east1.id  
    route   {
         cidr_block = "0.0.0.0/0"
         gateway_id = aws_internet_gateway.gweast1.id
    }
  
}

# associate each subnet  with route table 

resource "aws_route_table_association" "asscte1a" {
    subnet_id =aws_subnet.subnt1a.id 
    route_table_id = aws_route_table.rteast1.id
  
}
resource "aws_route_table_association" "asscte1b" {
    subnet_id =aws_subnet.subnt1b.id 
    route_table_id = aws_route_table.rteast1.id
  
}
resource "aws_route_table_association" "asscte1c" {
    subnet_id =aws_subnet.subnt1c.id 
    route_table_id = aws_route_table.rteast1.id
  
}
resource "aws_route_table_association" "asscte1d" {
    subnet_id =aws_subnet.subnt1d.id 
    route_table_id = aws_route_table.rteast1.id
  
}

# create nacl  for each zone.

resource "aws_network_acl" "nacl1a" {
    vpc_id = aws_vpc.vpc_east1.id

    egress  {
      action = "deny"
      cidr_block = "0.0.0.0/0"
      from_port = 0
      to_port = 0
      protocol = "-1"
      rule_no = 10
    } 
    ingress  {
      action = "allow"
      cidr_block = var.subnets_list[0]
      from_port = 80
      protocol = "tcp"
      rule_no = 10
      to_port = 80
    } 
    ingress  {
      action = "allow"
      cidr_block = var.subnets_list[0]
      from_port = 443
      protocol = "tcp"
      rule_no = 11
      to_port = 443
    } 
     ingress  {
      action = "allow"
      cidr_block = var.subnets_list[0]
      from_port = 22
      protocol = "tcp"
      rule_no = 12
      to_port = 22
    } 
 
}

resource "aws_network_acl" "nacl1b" {
    vpc_id = aws_vpc.vpc_east1.id

    egress  {
      action = "deny"
      cidr_block = "0.0.0.0/0"
      from_port = 0
      to_port = 0
      protocol = "-1"
      rule_no = 10
    } 
    ingress  {
      action = "allow"
      cidr_block = var.subnets_list[1]
      from_port = 80
      protocol = "tcp"
      rule_no = 10
      to_port = 80
    } 
    ingress  {
      action = "allow"
      cidr_block = var.subnets_list[1]
      from_port = 443
      protocol = "tcp"
      rule_no = 11
      to_port = 443
    } 
     ingress  {
      action = "allow"
      cidr_block = var.subnets_list[1]
      from_port = 22
      protocol = "tcp"
      rule_no = 12
      to_port = 22
    } 
 
}

resource "aws_network_acl" "nacl1c" {
    vpc_id = aws_vpc.vpc_east1.id

    egress  {
      action = "deny"
      cidr_block = "0.0.0.0/0"
      from_port = 0
      to_port = 0
      protocol = "-1"
      rule_no = 10
    } 
    ingress  {
      action = "allow"
      cidr_block = var.subnets_list[2]
      from_port = 80
      protocol = "tcp"
      rule_no = 10
      to_port = 80
    } 
    ingress  {
      action = "allow"
      cidr_block = var.subnets_list[2]
      from_port = 443
      protocol = "tcp"
      rule_no = 11
      to_port = 443
    } 
     ingress  {
      action = "allow"
      cidr_block = var.subnets_list[2]
      from_port = 22
      protocol = "tcp"
      rule_no = 12
      to_port = 22
    } 
 
}

resource "aws_network_acl" "nacl1d" {
    vpc_id = aws_vpc.vpc_east1.id

    egress  {
      action = "deny"
      cidr_block = "0.0.0.0/0"
      from_port = 0
      to_port = 0
      protocol = "-1"
      rule_no = 10
    } 
    ingress  {
      action = "allow"
      cidr_block = var.subnets_list[3]
      from_port = 80
      protocol = "tcp"
      rule_no = 10
      to_port = 80
    } 
    ingress  {
      action = "allow"
      cidr_block = var.subnets_list[3]
      from_port = 443
      protocol = "tcp"
      rule_no = 11
      to_port = 443
    } 
     ingress  {
      action = "allow"
      cidr_block = var.subnets_list[3]
      from_port = 22
      protocol = "tcp"
      rule_no = 12
      to_port = 22
    } 
 
}
# create  security group for each  zone 

resource "aws_security_group" "sec1a" {
    name = "sec_east1a"
    vpc_id = aws_vpc.vpc_east1.id
    ingress  {
      cidr_blocks = [ var.subnets_list[0] ]
      from_port = 80
      protocol = "tcp"
      to_port = 80
    } 
    ingress  {
      cidr_blocks = [ var.subnets_list[0] ]
      from_port = 443
      protocol = "tcp"
      to_port = 443
    } 
    ingress  {
      cidr_blocks = [ var.subnets_list[0] ]
      from_port = 22
      protocol = "tcp"
      to_port = 22
    } 
    egress  {
      cidr_blocks = [ "0.0.0.0/0" ]
      from_port = 0
      protocol = "-1"
      to_port = 0
    } 
}
resource "aws_security_group" "sec1b" {
    name = "sec_east1b"
    vpc_id = aws_vpc.vpc_east1.id
    ingress  {
      cidr_blocks = [ var.subnets_list[1] ]
      from_port = 80
      protocol = "tcp"
      to_port = 80
    } 
    ingress  {
      cidr_blocks = [ var.subnets_list[1] ]
      from_port = 443
      protocol = "tcp"
      to_port = 443
    } 
    ingress  {
      cidr_blocks = [ var.subnets_list[1] ]
      from_port = 22
      protocol = "tcp"
      to_port = 22
    } 
    egress  {
      cidr_blocks = [ "0.0.0.0/0" ]
      from_port = 0
      protocol = "-1"
      to_port = 0
    } 
}
resource "aws_security_group" "sec1c" {
    name = "sec_east1c"
    vpc_id = aws_vpc.vpc_east1.id
    ingress  {
      cidr_blocks = [ var.subnets_list[2] ]
      from_port = 80
      protocol = "tcp"
      to_port = 80
    } 
    ingress  {
      cidr_blocks = [ var.subnets_list[2] ]
      from_port = 443
      protocol = "tcp"
      to_port = 443
    } 
    ingress  {
      cidr_blocks = [ var.subnets_list[2] ]
      from_port = 22
      protocol = "tcp"
      to_port = 22
    } 
    egress  {
      cidr_blocks = [ "0.0.0.0/0" ]
      from_port = 0
      protocol = "-1"
      to_port = 0
    } 
}
resource "aws_security_group" "sec1d" {
    name = "sec_east1d"
    vpc_id = aws_vpc.vpc_east1.id
    ingress  {
      cidr_blocks = [ var.subnets_list[3] ]
      from_port = 80
      protocol = "tcp"
      to_port = 80
    } 
    ingress  {
      cidr_blocks = [ var.subnets_list[3] ]
      from_port = 443
      protocol = "tcp"
      to_port = 443
    } 
    ingress  {
      cidr_blocks = [ var.subnets_list[3] ]
      from_port = 22
      protocol = "tcp"
      to_port = 22
    } 
    egress  {
      cidr_blocks = [ "0.0.0.0/0" ]
      from_port = 0
      protocol = "-1"
      to_port = 0
    } 
}

# create   network interfaces for each ec2 instances 

resource "aws_network_interface" "intf1_est1a" {
    subnet_id = aws_subnet.subnt1a.id 
    private_ips = [var.ipaddr_list_est1a[0]]
    security_groups = [ aws_security_group.sec1a.id]
}
resource "aws_network_interface" "intf2_est1a" {
    subnet_id = aws_subnet.subnt1a.id 
    private_ips = [var.ipaddr_list_est1a[1]]
    security_groups = [ aws_security_group.sec1a.id]
}
resource "aws_network_interface" "intf3_est1a" {
    subnet_id = aws_subnet.subnt1a.id 
    private_ips = [var.ipaddr_list_est1a[2]]
    security_groups = [ aws_security_group.sec1a.id]
}
resource "aws_network_interface" "intf4_est1a" {
    subnet_id = aws_subnet.subnt1a.id 
    private_ips = [var.ipaddr_list_est1a[3]]
    security_groups = [ aws_security_group.sec1a.id]
}
resource "aws_network_interface" "intf1_est1b" {
    subnet_id = aws_subnet.subnt1a.id 
    private_ips = [var.ipaddr_list_est1b[0]]
    security_groups = [ aws_security_group.sec1b.id]
}
resource "aws_network_interface" "intf2_est1b" {
    subnet_id = aws_subnet.subnt1b.id 
    private_ips = [var.ipaddr_list_est1b[1]]
    security_groups = [ aws_security_group.sec1b.id]
}
resource "aws_network_interface" "intf3_est1b" {
    subnet_id = aws_subnet.subnt1b.id 
    private_ips = [var.ipaddr_list_est1b[2]]
    security_groups = [ aws_security_group.sec1b.id]
}
resource "aws_network_interface" "intf4_est1b" {
    subnet_id = aws_subnet.subnt1b.id 
    private_ips = [var.ipaddr_list_est1b[3]]
    security_groups = [ aws_security_group.sec1b.id]
}
resource "aws_network_interface" "intf1_est1c" {
    subnet_id = aws_subnet.subnt1c.id 
    private_ips = [var.ipaddr_list_est1c[0]]
    security_groups = [ aws_security_group.sec1c.id]
}
resource "aws_network_interface" "intf2_est1c" {
    subnet_id = aws_subnet.subnt1c.id 
    private_ips = [var.ipaddr_list_est1c[1]]
    security_groups = [ aws_security_group.sec1c.id]
}
resource "aws_network_interface" "intf3_est1c" {
    subnet_id = aws_subnet.subnt1c.id 
    private_ips = [var.ipaddr_list_est1c[2]]
    security_groups = [ aws_security_group.sec1c.id]
}
resource "aws_network_interface" "intf4_est1c" {
    subnet_id = aws_subnet.subnt1c.id 
    private_ips = [var.ipaddr_list_est1c[3]]
    security_groups = [ aws_security_group.sec1c.id]
}
resource "aws_network_interface" "intf1_est1d" {
    subnet_id = aws_subnet.subnt1d.id 
    private_ips = [var.ipaddr_list_est1d[0]]
    security_groups = [ aws_security_group.sec1d.id]
}
resource "aws_network_interface" "intf2_est1d" {
    subnet_id = aws_subnet.subnt1d.id 
    private_ips = [var.ipaddr_list_est1d[1]]
    security_groups = [ aws_security_group.sec1d.id]
}
resource "aws_network_interface" "intf3_est1d" {
    subnet_id = aws_subnet.subnt1d.id 
    private_ips = [var.ipaddr_list_est1d[2]]
    security_groups = [ aws_security_group.sec1d.id]
}
resource "aws_network_interface" "intf4_est1d" {
    subnet_id = aws_subnet.subnt1d.id 
    private_ips = [var.ipaddr_list_est1d[3]]
    security_groups = [ aws_security_group.sec1d.id]
}

# create  eip for each ec2 instance  in  zones

resource "aws_eip" "eip1_east1a" {
    vpc = true 
    network_interface  = aws_network_interface.intf1_est1a.id
    instance = aws_instance.east1a_inst1[0].id
    depends_on = [ aws_internet_gateway.gweast1 ]
}
resource "aws_eip" "eip2_east1a" {
    vpc = true 
    network_interface  = aws_network_interface.intf2_est1a.id
    instance = aws_instance.east1a_inst1[1].id
    depends_on = [ aws_internet_gateway.gweast1 ]
}
resource "aws_eip" "eip3_east1a" {
    vpc = true 
    network_interface  = aws_network_interface.intf3_est1a.id
    instance = aws_instance.east1a_inst1[2].id
    depends_on = [ aws_internet_gateway.gweast1 ]
}
resource "aws_eip" "eip4_east1a" {
    vpc = true 
    network_interface  = aws_network_interface.intf4_est1a.id
    instance = aws_instance.east1a_inst1[3].id
    depends_on = [ aws_internet_gateway.gweast1 ]
}
resource "aws_eip" "eip1_east1b" {
    vpc = true 
    network_interface  = aws_network_interface.intf1_est1b.id
    instance = aws_instance.east1b_inst1[0].id
    depends_on = [ aws_internet_gateway.gweast1 ]
}
resource "aws_eip" "eip2_east1b" {
    vpc = true 
    network_interface  = aws_network_interface.intf2_est1b.id
    instance = aws_instance.east1b_inst1[1].id
    depends_on = [ aws_internet_gateway.gweast1 ]
}
resource "aws_eip" "eip3_east1b" {
    vpc = true 
    network_interface  = aws_network_interface.intf3_est1b.id
    instance = aws_instance.east1b_inst1[2].id
    depends_on = [ aws_internet_gateway.gweast1 ]
}
resource "aws_eip" "eip4_east1b" {
    vpc = true 
    network_interface  = aws_network_interface.intf4_est1b.id
    instance = aws_instance.east1b_inst1[3].id
    depends_on = [ aws_internet_gateway.gweast1 ]
}

resource "aws_eip" "eip1_east1c" {
    vpc = true 
    network_interface  = aws_network_interface.intf1_est1c.id
    instance = aws_instance.east1c_inst1[0].id
    depends_on = [ aws_internet_gateway.gweast1 ]
}
resource "aws_eip" "eip2_east1c" {
    vpc = true 
    network_interface  = aws_network_interface.intf2_est1c.id
    instance = aws_instance.east1c_inst1[1].id
    depends_on = [ aws_internet_gateway.gweast1 ]
}
resource "aws_eip" "eip3_east1c" {
    vpc = true 
    network_interface  = aws_network_interface.intf3_est1c.id
    instance = aws_instance.east1c_inst1[2].id
    depends_on = [ aws_internet_gateway.gweast1 ]
}
resource "aws_eip" "eip4_east1c" {
    vpc = true 
    network_interface  = aws_network_interface.intf4_est1c.id
    instance = aws_instance.east1c_inst1[3].id
    depends_on = [ aws_internet_gateway.gweast1 ]
}

resource "aws_eip" "eip1_east1d" {
    vpc = true 
    network_interface  = aws_network_interface.intf1_est1d.id
    instance = aws_instance.east1d_inst1[0].id
    depends_on = [ aws_internet_gateway.gweast1 ]
}
resource "aws_eip" "eip2_east1d" {
    vpc = true 
    network_interface  = aws_network_interface.intf2_est1d.id
    instance = aws_instance.east1d_inst1[1].id
    depends_on = [ aws_internet_gateway.gweast1 ]
}
resource "aws_eip" "eip3_east1d" {
    vpc = true 
    network_interface  = aws_network_interface.intf3_est1d.id
    instance = aws_instance.east1d_inst1[2].id
    depends_on = [ aws_internet_gateway.gweast1 ]
}
resource "aws_eip" "eip4_east1d" {
    vpc = true 
    network_interface  = aws_network_interface.intf4_est1d.id
    instance = aws_instance.east1d_inst1[3].id
    depends_on = [ aws_internet_gateway.gweast1 ]
}

# create ebs volume and  for each zone to attach with ec2 instances 

resource "aws_ebs_volume" "ebs1_east1a" {
    size = 10
    availability_zone = var.zone_lists[0]
}
resource "aws_ebs_volume" "ebs2_east1b" {
    size = 10
    availability_zone = var.zone_lists[1]
}
resource "aws_ebs_volume" "ebs3_east1c" {
    size = 10
    availability_zone = var.zone_lists[2]
}
resource "aws_ebs_volume" "ebs4_east1d" {
    size = 10
    availability_zone = var.zone_lists[3]
}
# create ebs snapshot  
resource "aws_ebs_snapshot" "bkup1_east1a" {
    volume_id = aws_ebs_volume.ebs1_east1a.id
}
resource "aws_ebs_snapshot" "bkup2_east1b" {
    volume_id = aws_ebs_volume.ebs2_east1b.id
}
resource "aws_ebs_snapshot" "bkup3_east1c" {
    volume_id = aws_ebs_volume.ebs3_east1c.id
}
resource "aws_ebs_snapshot" "bkup4_east1d" {
    volume_id = aws_ebs_volume.ebs4_east1d.id
}


resource "aws_ebs_volume" "ebs1_est1a" {
    snapshot_id = aws_ebs_snapshot.bkup1_east1a.id
    availability_zone = var.zone_lists[0]
}
resource "aws_ebs_volume" "ebs2_est1a" {
    snapshot_id = aws_ebs_snapshot.bkup1_east1a.id
    availability_zone = var.zone_lists[0]
}
resource "aws_ebs_volume" "ebs3_est1a" {
    snapshot_id = aws_ebs_snapshot.bkup1_east1a.id
    availability_zone = var.zone_lists[0]
}
resource "aws_ebs_volume" "ebs1_est1b" {
    snapshot_id = aws_ebs_snapshot.bkup2_east1b.id
    availability_zone = var.zone_lists[1]
}
resource "aws_ebs_volume" "ebs2_est1b" {
    snapshot_id = aws_ebs_snapshot.bkup2_east1b.id
    availability_zone = var.zone_lists[1]
}
resource "aws_ebs_volume" "ebs3_est1b" {
    snapshot_id = aws_ebs_snapshot.bkup2_east1b.id
    availability_zone = var.zone_lists[1]
}
resource "aws_ebs_volume" "ebs1_est1c" {
    snapshot_id = aws_ebs_snapshot.bkup3_east1c.id
    availability_zone = var.zone_lists[2]
}
resource "aws_ebs_volume" "ebs2_est1c" {
    snapshot_id = aws_ebs_snapshot.bkup3_east1c.id
    availability_zone = var.zone_lists[2]
}
resource "aws_ebs_volume" "ebs3_est1c" {
    snapshot_id = aws_ebs_snapshot.bkup3_east1c.id
    availability_zone = var.zone_lists[2]
}
resource "aws_ebs_volume" "ebs1_est1d" {
    snapshot_id = aws_ebs_snapshot.bkup4_east1d.id
    availability_zone = var.zone_lists[3]
}
resource "aws_ebs_volume" "ebs2_est1d" {
    snapshot_id = aws_ebs_snapshot.bkup4_east1d.id
    availability_zone = var.zone_lists[3]
}
resource "aws_ebs_volume" "ebs3_est1d" {
    snapshot_id = aws_ebs_snapshot.bkup4_east1d.id
    availability_zone = var.zone_lists[3]
}

# attach ebs to  each ec2 instances  

resource "aws_volume_attachment" "attach10a"  {
    device_name = "/dev/sda1"
    volume_id = aws_ebs_volume.ebs1_east1a.id
    instance_id =aws_instance.east1a_inst1[0].id
}
resource "aws_volume_attachment" "attach11a"  {
    device_name = "/dev/sda2"
    volume_id = aws_ebs_volume.ebs1_est1a.id
    instance_id =aws_instance.east1a_inst1[1].id
}
resource "aws_volume_attachment" "attach12a"  {
    device_name = "/dev/sda3"
    volume_id = aws_ebs_volume.ebs2_est1a.id
    instance_id =aws_instance.east1a_inst1[2].id
}
resource "aws_volume_attachment" "attach13a"  {
    device_name = "/dev/sda3"
    volume_id = aws_ebs_volume.ebs3_est1a.id
    instance_id =aws_instance.east1a_inst1[3].id
}
resource "aws_volume_attachment" "attach20b"  {
    device_name = "/dev/sda4"
    volume_id = aws_ebs_volume.ebs2_east1b.id
    instance_id =aws_instance.east1b_inst1[0].id
}
resource "aws_volume_attachment" "attach21b"  {
    device_name = "/dev/sda5"
    volume_id = aws_ebs_volume.ebs1_est1b.id
    instance_id =aws_instance.east1b_inst1[1].id
}
resource "aws_volume_attachment" "attach22b"  {
    device_name = "/dev/sda6"
    volume_id = aws_ebs_volume.ebs2_est1b.id
    instance_id =aws_instance.east1b_inst1[2].id
}
resource "aws_volume_attachment" "attach23b"  {
    device_name = "/dev/sda7"
    volume_id = aws_ebs_volume.ebs3_est1b.id
    instance_id =aws_instance.east1b_inst1[3].id
}
resource "aws_volume_attachment" "attach30c"  {
    device_name = "/dev/sda8"
    volume_id = aws_ebs_volume.ebs3_east1c.id
    instance_id =aws_instance.east1c_inst1[0].id
}
resource "aws_volume_attachment" "attach31c"  {
    device_name = "/dev/sda9"
    volume_id = aws_ebs_volume.ebs1_est1c.id
    instance_id =aws_instance.east1c_inst1[1].id
}
resource "aws_volume_attachment" "attach32c"  {
    device_name = "/dev/sda10"
    volume_id = aws_ebs_volume.ebs2_est1c.id
    instance_id =aws_instance.east1c_inst1[2].id
}
resource "aws_volume_attachment" "attach33c"  {
    device_name = "/dev/sda11"
    volume_id = aws_ebs_volume.ebs3_est1c.id
    instance_id =aws_instance.east1c_inst1[3].id
}
resource "aws_volume_attachment" "attach40d"  {
    device_name = "/dev/sda12"
    volume_id = aws_ebs_volume.ebs4_east1d.id
    instance_id =aws_instance.east1d_inst1[0].id
}
resource "aws_volume_attachment" "attach41d"  {
    device_name = "/dev/sda12"
    volume_id = aws_ebs_volume.ebs1_est1d.id
    instance_id =aws_instance.east1d_inst1[1].id
}
resource "aws_volume_attachment" "attach42d"  {
    device_name = "/dev/sda13"
    volume_id = aws_ebs_volume.ebs2_est1d.id
    instance_id =aws_instance.east1d_inst1[2].id
}
resource "aws_volume_attachment" "attach43d"  {
    device_name = "/dev/sda14"
    volume_id = aws_ebs_volume.ebs3_est1d.id
    instance_id =aws_instance.east1d_inst1[3].id
}

#user-data =>> EOF
#!Bash/bin 
#sudo  yum update -y 
#sudo  yum install apache2 -y 
#sudo systemctl start httpd 
#sudo bash echo "c welcome to webserver c" /var/www/html/index.html















