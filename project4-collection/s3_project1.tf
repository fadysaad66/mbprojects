provider "aws" {
    alias = "east1"
    region = "us-east-1"
}
# create  s3 buckect  
resource "aws_s3_bucket" "bkeast1" { 
    bucket = "east1_lists"
    acl = "private" 
    versioning {
      enabled = true 
    }
    lifecycle_rule {
      enabled =true  
      
      transition {
        storage_class = "STANDARD_IA"
        days = 35
      }
      expiration {
        days = 365
      }
    }
    logging {
      target_bucket = aws_s3_bucket.bcket_log.id
    }
}

resource "aws_s3_bucket" "bcket_log" { 
    bucket = "bklog"
    acl = "log-delivery-write"
    versioning {
        enabled = true 
    }
    lifecycle_rule {
      enabled = true
      transition {
        days = 35
        storage_class = "STANDARD_IA"
      }
      transition {
        days = 95
        storage_class = "GLACIER"
      }
      expiration {
        days = 365
      }
    }

}