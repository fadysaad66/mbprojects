provider "aws" {
    alias = "east2"
    region = "us-east-1"

}
# create  app load balancer 

resource "aws_lb" "east1_apps" {
 name = "est1lb"
 load_balancer_type = "application"
enable_cross_zone_load_balancing = false
enable_http2 = true
enable_deletion_protection = false

subnet_mapping {
  allocation_id =aws_eip.eip1_east1a.id
  subnet_id = aws_subnet.subnt1a.id
}
subnet_mapping {
  allocation_id =aws_eip.eip2_east1a.id
  subnet_id = aws_subnet.subnt1a.id
}
subnet_mapping {
  allocation_id =aws_eip.eip3_east1a.id
  subnet_id = aws_subnet.subnt1a.id
}
subnet_mapping {
  allocation_id =aws_eip.eip4_east1a.id
  subnet_id = aws_subnet.subnt1a.id
}
subnet_mapping {
  allocation_id =aws_eip.eip1_east1b.id
  subnet_id = aws_subnet.subnt1b.id
}

subnet_mapping {
  allocation_id =aws_eip.eip2_east1b.id
  subnet_id = aws_subnet.subnt1b.id
}
subnet_mapping {
  allocation_id =aws_eip.eip3_east1b.id
  subnet_id = aws_subnet.subnt1b.id
}
subnet_mapping {
  allocation_id =aws_eip.eip4_east1b.id
  subnet_id = aws_subnet.subnt1b.id
}
subnet_mapping {
  allocation_id =aws_eip.eip1_east1c.id
  subnet_id = aws_subnet.subnt1c.id
}
subnet_mapping {
  allocation_id =aws_eip.eip2_east1c.id
  subnet_id = aws_subnet.subnt1c.id
}
subnet_mapping {
  allocation_id =aws_eip.eip3_east1c.id
  subnet_id = aws_subnet.subnt1c.id
}
subnet_mapping {
  allocation_id =aws_eip.eip4_east1c.id
  subnet_id = aws_subnet.subnt1c.id
}
subnet_mapping {
  allocation_id =aws_eip.eip1_east1d.id
  subnet_id = aws_subnet.subnt1d.id
}
subnet_mapping {
  allocation_id =aws_eip.eip2_east1d.id
  subnet_id = aws_subnet.subnt1d.id
}
subnet_mapping {
  allocation_id =aws_eip.eip3_east1d.id
  subnet_id = aws_subnet.subnt1d.id
}
subnet_mapping {
  allocation_id =aws_eip.eip4_east1d.id
  subnet_id = aws_subnet.subnt1d.id
}
access_logs {
  bucket = aws_s3_bucket.bcket_log.id
  prefix = "lblog"
}
security_groups = [ aws_security_group.sec_est1lb.id ]
}

# create  security-group   for load balancer
resource "aws_security_group" "sec_est1lb" {
    name = "scgp_est1_lb"
    vpc_id = aws_vpc.vpc_east1.id
    ingress  {
      cidr_blocks = [ "0.0.0.0/0" ]
      from_port = 80
      protocol = "tcp"
      to_port = 80
    } 
    ingress  {
      cidr_blocks = [ "0.0.0.0/0" ]
      from_port = 443
      protocol = "tcp"
      to_port = 443
    } 
    ingress  {
      cidr_blocks = [ "0.0.0.0/0" ]
      from_port = 22
      protocol = "tcp"
      to_port = 22
    } 
    egress  {
      cidr_blocks = [ "0.0.0.0/0" ]
      from_port = 0
      protocol = "-1"
      to_port = 0
    } 
}

# create lb listener  
resource "aws_lb_listener" "lis_port1" {
  load_balancer_arn = aws_lb.east1_apps.arn
  port = 80 
  protocol = "HTTP"
  default_action {
    type = "forward"
    target_group_arn =aws_lb_target_group.trgt1.arn
  }

}
resource "aws_lb_listener" "lis_port2" {
  load_balancer_arn = aws_lb.east1_apps.arn 
  port = 443 
  protocol = "HTTPS"
  default_action {
    type = "forward" 
    target_group_arn = aws_lb_target_group.trgt2.arn
  }

}
resource "aws_lb_target_group" "trgt1" {
  vpc_id=aws_vpc.vpc_east1.id 
  port = 80
  protocol = "HTTP"
  load_balancing_algorithm_type = "round_robin"
  target_type = "instance"
}
resource "aws_lb_target_group" "trgt2" {
  vpc_id = aws_vpc.vpc_east1.id 
  port = 80
  protocol = "HTTPS"
  load_balancing_algorithm_type = "round_robin"
  target_type = "instance"
  
}


# create  network load balancer 

resource "aws_lb" "ntlb_east1" {
  load_balancer_type = "network"
  enable_cross_zone_load_balancing = true
  enable_deletion_protection = true 
  

subnet_mapping {
  allocation_id =aws_eip.eip1_east1a.id
  subnet_id = aws_subnet.subnt1a.id
}
subnet_mapping {
  allocation_id =aws_eip.eip2_east1a.id
  subnet_id = aws_subnet.subnt1a.id
}
subnet_mapping {
  allocation_id =aws_eip.eip3_east1a.id
  subnet_id = aws_subnet.subnt1a.id
}
subnet_mapping {
  allocation_id =aws_eip.eip4_east1a.id
  subnet_id = aws_subnet.subnt1a.id
}
subnet_mapping {
  allocation_id =aws_eip.eip1_east1b.id
  subnet_id = aws_subnet.subnt1b.id
}

subnet_mapping {
  allocation_id =aws_eip.eip2_east1b.id
  subnet_id = aws_subnet.subnt1b.id
}
subnet_mapping {
  allocation_id =aws_eip.eip3_east1b.id
  subnet_id = aws_subnet.subnt1b.id
}
subnet_mapping {
  allocation_id =aws_eip.eip4_east1b.id
  subnet_id = aws_subnet.subnt1b.id
}
subnet_mapping {
  allocation_id =aws_eip.eip1_east1c.id
  subnet_id = aws_subnet.subnt1c.id
}
subnet_mapping {
  allocation_id =aws_eip.eip2_east1c.id
  subnet_id = aws_subnet.subnt1c.id
}
subnet_mapping {
  allocation_id =aws_eip.eip3_east1c.id
  subnet_id = aws_subnet.subnt1c.id
}
subnet_mapping {
  allocation_id =aws_eip.eip4_east1c.id
  subnet_id = aws_subnet.subnt1c.id
}
subnet_mapping {
  allocation_id =aws_eip.eip1_east1d.id
  subnet_id = aws_subnet.subnt1d.id
}
subnet_mapping {
  allocation_id =aws_eip.eip2_east1d.id
  subnet_id = aws_subnet.subnt1d.id
}
subnet_mapping {
  allocation_id =aws_eip.eip3_east1d.id
  subnet_id = aws_subnet.subnt1d.id
}
subnet_mapping {
  allocation_id =aws_eip.eip4_east1d.id
  subnet_id = aws_subnet.subnt1d.id
}
security_groups = [ aws_security_group.sec_est1lb.id ]
access_logs {
  bucket = aws_s3_bucket.bcket_log.id
  prefix = "ntlog"
}

}

# create network lb  listener  
resource "aws_lb_listener" "lis_nt10" {
  load_balancer_arn = aws_lb.east1_apps.arn
  port = 80 
  protocol = "HTTP"
  default_action {
    type = "forward"
    target_group_arn =aws_lb_target_group.trgt10.arn
  }

}
resource "aws_lb_listener" "lis_nt20" {
  load_balancer_arn = aws_lb.east1_apps.arn 
  port = 443 
  protocol = "HTTPS"
  default_action {
    type = "forward" 
    target_group_arn = aws_lb_target_group.trgt20.arn
  }

}
resource "aws_lb_listener" "lis_nt30" {
  load_balancer_arn = aws_lb.east1_apps.arn 
  port = 22
  protocol = "tcp"
  default_action {
    type = "forward" 
    target_group_arn = aws_lb_target_group.trgt30.arn
  }

}
# create target_group for network loadbalancer  

resource "aws_lb_target_group" "trgt10" {
  vpc_id=aws_vpc.vpc_east1.id 
  port = 80
  protocol = "HTTP"
  load_balancing_algorithm_type = "round_robin"
  target_type = "instance"
}
resource "aws_lb_target_group" "trgt20" {
  vpc_id = aws_vpc.vpc_east1.id 
  port = 80
  protocol = "HTTPS"
  load_balancing_algorithm_type = "round_robin"
  target_type = "instance"
  
}
resource "aws_lb_target_group" "trgt30" {
  vpc_id = aws_vpc.vpc_east1.id 
  port = 22
  protocol = "tcp"
  load_balancing_algorithm_type = "least_outstanding_requests"
  target_type = "instance"
}


