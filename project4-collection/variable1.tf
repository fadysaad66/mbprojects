
variable "tools" {
    type = map
    description = "tools"
    default = {
        "ami_list"  = "ami-01ca03df4a6012157"
        "type_list" = "t2.micro"
        "keys"      =  "kyn6690"
    }
}
variable "vpcs_list" {
    type = string
    description = "myrange"
    default = "80.1.0.0/16"
}
variable "subnets_list" {
    type = list
    description = "subnets"
    default =  [ "80.1.1.0/24" , "80.1.2.0/24" , "80.1.3.0/24" , "80.1.4.0/24" ]
}
variable "ipaddr_list_est1a" {
    type = list
    description = " addresses"
    default =  [ "80.1.1.10" ,"80.1.1.11","80.1.1.12", "80.1.1.14"]
}
variable "ipaddr_list_est1b" {
    type = list
    description = "addresses"
    default =  [ "80.1.2.20","80.1.2.21","80.1.2.22","80.1.2.23"]
}
variable "ipaddr_list_est1c" {
    type = list
    description = " addresses"
    default =  [ "80.1.3.30","80.1.3.31","80.1.3.32","80.1.3.33"]
}
variable "ipaddr_list_est1d" {
    type = list
    description = " addresses"
    default =  [ "80.1.4.40","80.1.4.41","80.1.4.42","80.1.4.43" ]
}
variable "zone_lists" {
    type = list
    description = "zones"
    default = [ "us-east-1a","us-east-1b","us-east-1c","us-east-1d"]
}
variable "names_list" {
    type = list
    description = "tags_list"
    default =  [ "east1a_webserver" , "east1b_webserver" , "east1c_webserver" ,"east1d_webserver" ]
}

output "tools_out" {
    value = var.tools
}
output "vpcs_out" {
    value = var.vpcs_list
}
output "subnets_out" {
    value = var.subnets_list
}
output "ipaddr_out_est1a" {
    value = var.ipaddr_list_est1a
}
output "ipaddr_out_est1b" {
    value = var.ipaddr_list_est1b
}
output "ipaddr_out_est1c" {
    value = var.ipaddr_list_est1c
}
output "ipaddr_out_est1d" {
    value = var.ipaddr_list_est1d
}
output "zones_out" {
    value = var.zone_lists
}
output "names_out" {
    value = var.names_list
}
